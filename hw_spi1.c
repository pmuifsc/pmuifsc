
#include "hw_spi1.h"

#include "stm32f4xx_conf.h"
#include "stm32f4xx/stm32f4xx_gpio.h"
#include "stm32f4xx/stm32f4xx_spi.h"

#define GPIO_CONTROLLED_SS 1

// SS_PIN configuration
#ifdef GPIO_CONTROLLED_SS
#define SS_PIN GPIO_Pin_15
#define SS_PORT GPIOA
#else
#define SS_PIN GPIO_Pin_15
#define SS_PORT GPIOA
#define SS_SOURCE_PIN GPIO_PinSource15
#endif

#define SCK_PORT GPIOB
#define SCK_SOURCE_PIN GPIO_PinSource3
#define SCK_PIN GPIO_Pin_3

#define MOSI_PORT GPIOB
#define MOSI_SOURCE_PIN GPIO_PinSource5
#define MOSI_PIN GPIO_Pin_5

#define MISO_PORT GPIOB
#define MISO_SOURCE_PIN GPIO_PinSource4
#define MISO_PIN GPIO_Pin_4


#define CSACTIVE() GPIO_ResetBits(SS_PORT, SS_PIN)
#define CSPASSIVE() GPIO_SetBits(SS_PORT, SS_PIN)

void hw_spi1_Init() {
  // TODO: Implement Interrputs and/or DMA
  // TODO: Implement hw_SPI1 to handle peripheral
  // Initialize peripheral clock
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  
  // Initialize GPIO_Interface
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_StructInit(&GPIO_InitStruct);
#ifdef GPIO_CONTROLLED_SS
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
#else
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
#endif
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStruct.GPIO_Pin = SS_PIN;
  GPIO_Init(SS_PORT, &GPIO_InitStruct);
  GPIO_Init(GPIOD, &GPIO_InitStruct);
  
  
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Pin = SCK_PIN;
  GPIO_Init(SCK_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.GPIO_Pin = MOSI_PIN;
  GPIO_Init(MOSI_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.GPIO_Pin = MISO_PIN;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
  GPIO_Init(MISO_PORT, &GPIO_InitStruct);

  /* ss is active low */
#ifndef GPIO_CONTROLLED_SS
  GPIO_PinAFConfig(SS_PORT, SS_SOURCE_PIN, GPIO_AF_SPI1);
#endif
  GPIO_PinAFConfig(SCK_PORT, SCK_SOURCE_PIN, GPIO_AF_SPI1);
  GPIO_PinAFConfig(MOSI_PORT, MOSI_SOURCE_PIN, GPIO_AF_SPI1);
  GPIO_PinAFConfig(MISO_PORT, MISO_SOURCE_PIN, GPIO_AF_SPI1);
  
  // Initialize SPI Peripheral
  SPI_InitTypeDef SPI_InitStructure;
  SPI_I2S_DeInit(SPI1);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
#ifdef GPIO_CONTROLLED_SS
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
#else
  SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
#endif
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8; // 8
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStructure);

  SPI_Cmd(SPI1, ENABLE);
  return;
}

void hw_spi1_lowSpeed() {
  SPI_InitTypeDef SPI_InitStructure;
  SPI_I2S_DeInit(SPI1);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
#ifdef GPIO_CONTROLLED_SS
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
#else
  SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
#endif
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; // 8
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStructure);

  SPI_Cmd(SPI1, ENABLE);
}

void hw_spi1_highSpeed() {
  SPI_InitTypeDef SPI_InitStructure;
  SPI_I2S_DeInit(SPI1);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
#ifdef GPIO_CONTROLLED_SS
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
#else
  SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
#endif
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8; // 8
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStructure);

  SPI_Cmd(SPI1, ENABLE);
}

void hw_spi1_set8bits(void) {
  SPI_Cmd(SPI1, DISABLE);
  SPI_DataSizeConfig(SPI1, SPI_DataSize_8b);
  SPI_Cmd(SPI1, ENABLE);
}

void hw_spi1_set16bits(void) {
  SPI_Cmd(SPI1, DISABLE);
  SPI_DataSizeConfig(SPI1, SPI_DataSize_16b);
  SPI_Cmd(SPI1, ENABLE);
}


void hw_spi1_destroy() {
  SPI_Cmd(SPI1, DISABLE);
}

void hw_spi1_select() {
  CSACTIVE();
  GPIO_SetBits(GPIOD, SS_PIN);
}

void hw_spi1_unselect() {
  CSPASSIVE();
  GPIO_ResetBits(GPIOD, SS_PIN);
}

char hw_spi1_send(uint16_t data) {
  uint16_t LIS302DLTimeout = 0x1000;

  while (SPI_I2S_GetFlagStatus(SPI1, SPI_FLAG_TXE) == RESET) {
    if((LIS302DLTimeout--) == 0) return -1; // error
  }
  SPI_I2S_SendData(SPI1, data);
  return 0;
}

uint16_t hw_spi1_read(void) {
  uint16_t LIS302DLTimeout = 0x1000;

  /* Wait to receive a Byte */
  while (SPI_I2S_GetFlagStatus(SPI1,  SPI_FLAG_RXNE) == RESET) {
    if((LIS302DLTimeout--) == 0) return 0x00; // error
  }
  return SPI_I2S_ReceiveData(SPI1);
}

uint16_t hw_spi1_exchange(uint16_t data) {
  if (hw_spi1_send(data))
    return 0x00;
  return hw_spi1_read();
}
