
#include "hw_adc.h"

#include <stdio.h>

#include <stm32f4xx.h>
#include <stm32f4xx/stm32f4xx_adc.h>
#include <stm32f4xx/stm32f4xx_gpio.h>
#include <stm32f4xx/stm32f4xx_dma.h>
#include <stm32f4xx/stm32f4xx_pwr.h>
#include <stm32f4xx/stm32f4xx_rcc.h>
#include <stm32f4xx/misc.h>

#include "hw_exti0.h"
#include "hw_timer8.h"
#include "hw_timer2.h"
#include "rtc.h"
#include "calculus.h"

volatile uint16_t adcBuffer[4][COUNTSAMPLES*3];
volatile uint8_t  lastUsedBuffer;
volatile uint8_t  dirtyBuffer[4];
volatile uint32_t bufferTimeStamp[4][2];
volatile float32_t diffPhaseInASec;
static uint8_t usedMemoryAddr;
volatile uint8_t samplesSynced;

// Private function declaration
void hw_dma_Init(void);
void hw_dma_Init_registers(void);
void hw_adc_optimize(FunctionalState NewState);

#define GPIO_PORT_R GPIOC
#define GPIO_PIN_R  GPIO_Pin_0
#define ADC_CHANNEL_R 10
#define GPIO_PORT_S GPIOC
#define GPIO_PIN_S  GPIO_Pin_2
#define ADC_CHANNEL_S 12
#define GPIO_PORT_T GPIOA
#define GPIO_PIN_T  GPIO_Pin_3
#define ADC_CHANNEL_T 3

// Default is 60
#define NVIC_PRIORITY 0
#define NVIC_IRQ DMA2_Stream4_IRQn


// As defined in AN4073
#define AN4073_OPITION  1

void hw_adc_optimize(FunctionalState NewState) {
#if AN4073_OPITION == 1
    /* ENABLE PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR,ENABLE);
  if (NewState != DISABLE)
  {
    /* Disable FLASH PREFETCH */
    FLASH->ACR &= ~(FLASH_ACR_PRFTEN);
    /* Set ADCDC1 bit */
    PWR->CR |= ((uint32_t)PWR_CR_ADCDC1);
  }
  else
  {
    /* Reset ADCDC1 bit */
    PWR->CR &= (uint32_t)(~PWR_CR_ADCDC1);
    /* Enable FLASH PREFETCH */
    FLASH->ACR |= FLASH_ACR_PRFTEN;
  }
  return;
#elif AN4073_OPITION == 2
    /* Enable the SYSCFG clock*/
  uint32_t ADCxDC2 = SYSCFG_PMC_ADCxDC2;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);
  if (NewState != DISABLE)
  {
    /* Set the ADCxDC2 */
    SYSCFG->PMC |= (uint32_t)ADCxDC2;
  }
  else
  {
    /* Reset the ADCxDC2 */
    SYSCFG->PMC &=(uint32_t)(~ADCxDC2);
  }
  return;
#else
  return;
#endif
}

void hw_adc_Init(float nominalFrequency) {
  ADC_InitTypeDef       ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  GPIO_InitTypeDef      GPIO_InitStructure;

  diffPhaseInASec = 0;
  samplesSynced = 0;

  /* Enable ADC, DMA2 clocks **************************************************/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_ADC2 |
  RCC_APB2Periph_ADC3, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

  /* Configure ADC pins, not needed configure AF, because are read only *******/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  GPIO_InitStructure.GPIO_Pin = GPIO_PIN_R;
  GPIO_Init(GPIO_PORT_R, &GPIO_InitStructure);
  GPIO_PinLockConfig(GPIO_PORT_R, GPIO_PIN_R);

  GPIO_InitStructure.GPIO_Pin = GPIO_PIN_S;
  GPIO_Init(GPIO_PORT_S, &GPIO_InitStructure);
  GPIO_PinLockConfig(GPIO_PORT_S, GPIO_PIN_S);

  GPIO_InitStructure.GPIO_Pin = GPIO_PIN_T;
  GPIO_Init(GPIO_PORT_T, &GPIO_InitStructure);
  GPIO_PinLockConfig(GPIO_PORT_T, GPIO_PIN_T);

  /****************************************************************************/
  /*  ADCs configuration: triple simultaneous mode                            */
  /****************************************************************************/

  /* ADC improvment based on AN4073 *******************************************/
  hw_adc_optimize(ENABLE);

  /* ADC Common configuration *************************************************/
  ADC_CommonInitStructure.ADC_Mode = ADC_TripleMode_RegSimult;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  /* ADC1 regular channel configuration ***************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T8_TRGO;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = 1;

  ADC_Init(ADC1, &ADC_InitStructure);
  ADC_RegularChannelConfig(ADC1, ADC_CHANNEL_R, 1, ADC_SampleTime_3Cycles);

  /* ADC2 regular channel configuration ***************************************/
  ADC_Init(ADC2, &ADC_InitStructure);
  ADC_RegularChannelConfig(ADC2, ADC_CHANNEL_S, 1, ADC_SampleTime_3Cycles);

  /* ADC3 regular channel configuration ***************************************/
  ADC_Init(ADC3, &ADC_InitStructure);
  ADC_RegularChannelConfig(ADC3, ADC_CHANNEL_T, 1, ADC_SampleTime_3Cycles);

  /* DMA configuration ********************************************************/
  /* Enable DMA request after last transfer (multi-ADC mode) ******************/
  ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);
  hw_dma_Init();

  /* Enable ADC1 **************************************************************/
  ADC_Cmd(ADC1, ENABLE);

  /* Enable ADC2 **************************************************************/
  ADC_Cmd(ADC2, ENABLE);

  /* Enable ADC3 **************************************************************/
  ADC_Cmd(ADC3, ENABLE);

#if 0
  // Need implement overrun errors

  // test propouse
  ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);
  ADC_ITConfig(ADC2, ADC_IT_EOC, ENABLE);
  ADC_ITConfig(ADC3, ADC_IT_EOC, ENABLE);

  NVIC_InitTypeDef NVIC_Struct;
  NVIC_Struct.NVIC_IRQChannel = ADC_IRQn;
  NVIC_Struct.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_Struct.NVIC_IRQChannelSubPriority = 0;
  NVIC_Struct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_Struct);
#endif

  GPIO_InitTypeDef gpiocfg;
  gpiocfg.GPIO_Pin = GPIO_Pin_7;
  gpiocfg.GPIO_Mode = GPIO_Mode_OUT;
  gpiocfg.GPIO_Speed = GPIO_Speed_100MHz;
  gpiocfg.GPIO_OType = GPIO_OType_PP;
  gpiocfg.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &gpiocfg);

  GPIO_WriteBit(GPIOA, GPIO_Pin_7, 1);

  /* Initialize timer 8 to trigger ADC ****************************************/
  hw_timer8_Init(nominalFrequency, COUNTSAMPLES);
  hw_timer8_start();
}

int32_t crystalErr = 0;
void hw_adc_reset(void) {
  crystalErr = TIMER2_BASE - hw_exti0_time();
  if (((crystalErr < -COUNTSAMPLES) || (crystalErr > COUNTSAMPLES)) && samplesSynced) {
    samplesSynced = 0;
  } else if (samplesSynced) {
    hw_timer8_set_err(crystalErr);
    hw_timer8_reset();
  } else if ((crystalErr >= -COUNTSAMPLES) && (crystalErr <= COUNTSAMPLES)) {
    hw_timer8_stop();
    hw_dma_Init_registers();
    hw_timer8_set_err(crystalErr);
    hw_timer8_restart();
    samplesSynced = 1;
  }
}

void hw_dma_Init() {
  lastUsedBuffer = 1;
  dirtyBuffer[0] = 1;
  dirtyBuffer[1] = 1;
  dirtyBuffer[2] = 1;
  dirtyBuffer[3] = 1;
  bufferTimeStamp[0][0] = 0;
  bufferTimeStamp[1][0] = 0;
  bufferTimeStamp[2][0] = 0;
  bufferTimeStamp[3][0] = 0;
  bufferTimeStamp[0][1] = 0;
  bufferTimeStamp[1][1] = 0;
  bufferTimeStamp[2][1] = 0;
  bufferTimeStamp[3][1] = 0;

  hw_dma_Init_registers();
}

void hw_dma_Init_registers() {
  NVIC_InitTypeDef      NVIC_Struct;
  DMA_InitTypeDef       DMA_INIT;

  usedMemoryAddr = 0;

  /* DMA configuration ********************************************************/
  DMA_Cmd(DMA2_Stream4, DISABLE);
  DMA_DeInit(DMA2_Stream4);
  DMA_INIT.DMA_Channel = DMA_Channel_0;
  DMA_INIT.DMA_PeripheralBaseAddr = (uint32_t) &(ADC->CDR);
  DMA_INIT.DMA_Memory0BaseAddr    = (uint32_t)&adcBuffer[(lastUsedBuffer-1) & 0x03][(COUNTSAMPLES * 3) / 2];
  DMA_INIT.DMA_DIR                = DMA_DIR_PeripheralToMemory;
  DMA_INIT.DMA_BufferSize         = (COUNTSAMPLES * 3) / 2; // 3 phase
  DMA_INIT.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
  DMA_INIT.DMA_MemoryInc          = DMA_MemoryInc_Enable;
  // Adc data register has 16 bits length
  DMA_INIT.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  // Buffer type is 16 bytes too (uint16_t)
  DMA_INIT.DMA_MemoryDataSize     = DMA_MemoryDataSize_HalfWord;
  DMA_INIT.DMA_Mode               = DMA_Mode_Circular;
  DMA_INIT.DMA_Priority           = DMA_Priority_VeryHigh;
  DMA_INIT.DMA_FIFOMode           = DMA_FIFOMode_Disable;
  DMA_INIT.DMA_FIFOThreshold      = DMA_FIFOThreshold_HalfFull;
  DMA_INIT.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
  DMA_INIT.DMA_PeripheralBurst    = DMA_PeripheralBurst_Single;

  // Double buffer functions must be called before DMA_Init()
  DMA_DoubleBufferModeConfig(DMA2_Stream4,
                             (uint32_t) adcBuffer[lastUsedBuffer & 0x03],
                             DMA_Memory_0);
  // Activate double buffer mode
  DMA_DoubleBufferModeCmd(DMA2_Stream4, ENABLE);
  // DMA interrupts after buffer got full
  DMA_ITConfig(DMA2_Stream4, DMA_IT_TC, ENABLE);
  DMA_ITConfig(DMA2_Stream4, DMA_IT_TE, ENABLE);

  DMA_Init(DMA2_Stream4, &DMA_INIT);

  DMA_Cmd(DMA2_Stream4, ENABLE);

  NVIC_Struct.NVIC_IRQChannel = DMA2_Stream4_IRQn;
  NVIC_Struct.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  NVIC_Struct.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  NVIC_Struct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_Struct);
}

#include "interrupt.h"

void DMA2_Stream4_IRQHandler(void) {
  // Exchange buffers
  if (DMA_GetITStatus(DMA2_Stream4, DMA_IT_TCIF4) == SET) {
    if (usedMemoryAddr) {
      uint32_t SOC, newSOC, prevSOC, fracSec, prevFracSec;
      SOC = rtc_getSOC();
      fracSec = hw_timer2_read() + (uint32_t)(TIMER2_BASE/(2*COUNTSAMPLES*F_FREQ));
      fracSec %= TIMER2_BASE;
      newSOC = rtc_getSOC();
      prevSOC = bufferTimeStamp[(lastUsedBuffer-1) & 0x03][STAMP_SOC];
      prevFracSec = bufferTimeStamp[(lastUsedBuffer-1) & 0x03][STAMP_FRAC];
      // Check if second was changed
      if (SOC != newSOC) {
        if (fracSec < 100) {
          SOC = newSOC;
        }
      } else if (SOC == prevSOC) {
        if (fracSec < prevFracSec) {
          SOC++;
        }
      }
      bufferTimeStamp[lastUsedBuffer & 0x03][STAMP_SOC] = SOC;
      // Less half time between samples
      bufferTimeStamp[lastUsedBuffer & 0x03][STAMP_FRAC] = fracSec;
      lastUsedBuffer++;
      dirtyBuffer[lastUsedBuffer & 0x03] = 1; // buffer will be used by DMA
      DMA_MemoryTargetConfig(DMA2_Stream4,
                              (uint32_t)&adcBuffer[lastUsedBuffer & 0x03][0],
                              1);
      usedMemoryAddr = 0;
    } else {
      DMA_MemoryTargetConfig(DMA2_Stream4,
                              (uint32_t)&adcBuffer[lastUsedBuffer & 0x03][(COUNTSAMPLES * 3) / 2],
                              0);
      usedMemoryAddr = 1;
    }
    DMA_ClearITPendingBit(DMA2_Stream4, DMA_IT_TCIF4);
  }
  if (DMA_GetITStatus(DMA2_Stream4, DMA_IT_TEIF4) == SET) {
    DMA_ClearITPendingBit(DMA2_Stream4, DMA_IT_TEIF4);
  }
}

#if 0
void ADC_IRQHandler(void) {
  // TODO Handle overrun error
}
#endif
