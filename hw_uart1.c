
#include "hw_uart1.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_usart.h"
#include "stm32f4xx/stm32f4xx_gpio.h"
#include "stm32f4xx/misc.h"

#include <stdarg.h>
#include <stdio.h>

#define UART_NUMBER     3

#if UART_NUMBER==1
  #define RX_PIN    GPIO_Pin_7
  #define RX_PIN_S  GPIO_PinSource7
  #define RX_PORT   GPIOB
  #define TX_PIN    GPIO_Pin_6
  #define TX_PIN_S  GPIO_PinSource6
  #define TX_PORT   GPIOB

  #define USART           USART1
  #define USART_BAUDRATE  2000000
  #define NVIC_IRQN       USART1_IRQn
#elif UART_NUMBER==3
  #define RX_PIN    GPIO_Pin_11
  #define RX_PIN_S  GPIO_PinSource11
  #define RX_PORT   GPIOB
  #define TX_PIN    GPIO_Pin_10
  #define TX_PIN_S  GPIO_PinSource10
  #define TX_PORT   GPIOB

  #define USART           USART3
  #define USART_BAUDRATE  2000000
  #define NVIC_IRQN       USART3_IRQn
#endif

// 37 is default value
#define NVIC_PRIORITY 7

static char wBuffer[256];
static char rBuffer[256];
volatile static uint8_t wHead, wTail;
volatile static uint8_t rHead, rTail;

#if 0
// Print formated definition not in use yet
void uart1Printf(char *tmpBuffer, const char *str, ...) {
  va_list arg;
  va_start (arg, str);
  vsprintf (tmpBuffer, str, arg);
  va_end (arg);
  return;
}
#endif

void uart1Print(const char *str) {
  for (;*str; str++) {
    hw_uart1_send(*str);
  }
}

void hw_uart1_Init() {
  NVIC_InitTypeDef nviccfg;
  GPIO_InitTypeDef gpiocfg;
  USART_InitTypeDef usartcfg;
  // Enable clock to Peripheral USART
#if UART_NUMBER==1
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
#elif UART_NUMBER==3
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
#endif
  // Initialize UART values
  USART_StructInit(&usartcfg);
  // Overwrite Default Baudrate. Default == 9600
  usartcfg.USART_BaudRate = USART_BAUDRATE;
  USART_Init(USART, &usartcfg);
  USART_Cmd(USART, ENABLE);
  USART_ITConfig(USART, USART_IT_RXNE, ENABLE);

  // GPIO Init
  GPIO_StructInit(&gpiocfg);
  gpiocfg.GPIO_Mode = GPIO_Mode_AF;
  gpiocfg.GPIO_Speed = GPIO_Speed_100MHz;
  // Set GPIO pins to USART RX Function
  gpiocfg.GPIO_Pin = RX_PIN;
  GPIO_Init(RX_PORT, &gpiocfg);
#if UART_NUMBER==1
  GPIO_PinAFConfig(RX_PORT, RX_PIN_S, GPIO_AF_USART1);
#elif UART_NUMBER==3
  GPIO_PinAFConfig(RX_PORT, RX_PIN_S, GPIO_AF_USART3);
#endif

  // Set GPIO pins to USART TX Function
  gpiocfg.GPIO_Pin = TX_PIN;
  GPIO_Init(TX_PORT, &gpiocfg);
#if UART_NUMBER==1
  GPIO_PinAFConfig(TX_PORT, TX_PIN_S, GPIO_AF_USART1);
#elif UART_NUMBER==3
  GPIO_PinAFConfig(TX_PORT, TX_PIN_S, GPIO_AF_USART3);
#endif

  // Ensure the pin configuration dont be overwrited by another peripheral
  // Comment if you want to disable uart and use those pins
  GPIO_PinLockConfig(TX_PORT, TX_PIN);
  GPIO_PinLockConfig(RX_PORT, RX_PIN);

  // Configure Interrupt priority in NVIC
  nviccfg.NVIC_IRQChannel = NVIC_IRQN;
  nviccfg.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  nviccfg.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  nviccfg.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nviccfg);

  // Object initializations
  wHead = 0;
  wTail = 0;
  rHead = 0;
  rTail = 0;
}

void hw_uart1_destroy() {
  NVIC_InitTypeDef nviccfg;
  nviccfg.NVIC_IRQChannel = NVIC_IRQN;
  nviccfg.NVIC_IRQChannelCmd = DISABLE;
  NVIC_Init(&nviccfg);
  USART_DeInit(USART);
  wHead = 0;
  wTail = 0;
  rHead = 0;
  rTail = 0;
}

char hw_uart1_send(unsigned char c) {
  if (wHead + 1 == wTail)
    return -1;
  USART_ITConfig(USART, USART_IT_TC, DISABLE);
  wBuffer[wHead++] = c;
  USART_ITConfig(USART, USART_IT_TC, ENABLE);
  return 0;
}

char hw_uart1_read(char *readed) {
  if (rHead == rTail) {
    *readed = 0;
    return -1;
  }
  USART_ITConfig(USART, USART_IT_RXNE, DISABLE);
  *readed = rBuffer[rTail++];
  USART_ITConfig(USART, USART_IT_RXNE, ENABLE);
  return 0;
}

#include "interrupt.h"

#if UART_NUMBER==1
void USART1_IRQHandler(void) {
#elif UART_NUMBER==3
void USART3_IRQHandler(void) {
#endif
  if (USART_GetFlagStatus(USART, USART_FLAG_RXNE) == SET) {
    char ch = USART_ReceiveData(USART);
    if (rHead + 1 != rTail) {
      rBuffer[rHead++] = ch;
    }
  }
  if (USART_GetFlagStatus(USART, USART_FLAG_TC) == SET) {
    if (wHead == wTail) {
      USART_ITConfig(USART, USART_IT_TC, DISABLE);
    } else {
      USART_SendData(USART, wBuffer[wTail++]);
    }
  }
}
