
#include "hw_exti0.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_exti.h"
#include "stm32f4xx/stm32f4xx_gpio.h"
#include "stm32f4xx/stm32f4xx_syscfg.h"
#include "stm32f4xx/misc.h"

#include "hw_adc.h"
#include "hw_timer2.h"
#include "hw_timer8.h"
#include <stdio.h>

#define GPIO_PORT GPIOD
#define EXTI_PORT EXTI_PortSourceGPIOD

#define GPIO_PIN  GPIO_Pin_7
#define EXTI_PIN  EXTI_PinSource7
#define EXTI_LINE EXTI_Line7

// Default is 13, for exti0
#define NVIC_PRIORITY 1
#define NVIC_IRQ  EXTI9_5_IRQn

int32_t diffTime;
volatile char diffTimeTrigger;

static void dummy(void) {
}

static void (*interruptCallBack)(void) = NULL;

void hw_exti0_Init() {
  NVIC_InitTypeDef nviccfg;
  EXTI_InitTypeDef exticfg;
  GPIO_InitTypeDef gpiocfg;

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Configure PA0 pin as input floating */
  gpiocfg.GPIO_Mode = GPIO_Mode_IN;
  gpiocfg.GPIO_PuPd = GPIO_PuPd_NOPULL;
  gpiocfg.GPIO_Pin  = GPIO_PIN;
  gpiocfg.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIO_PORT, &gpiocfg);

  // Ensure the pin configuration dont be overwrited by another peripheral
  // Comment if you want to disable uart and use those pins
  GPIO_PinLockConfig(GPIO_PORT, GPIO_PIN);

  /* Connect EXTI Line(EXTI_PIN) to Port(EXTI_PORT)(EXTI_PIN) pin*/
  SYSCFG_EXTILineConfig(EXTI_PORT, EXTI_PIN);

  /* Configure EXTI Line0 */
  exticfg.EXTI_Line = EXTI_LINE;
  exticfg.EXTI_Mode = EXTI_Mode_Interrupt;
  exticfg.EXTI_Trigger = EXTI_Trigger_Rising;
  exticfg.EXTI_LineCmd = ENABLE;
  EXTI_Init(&exticfg);

  // define a dummy callback function
  if (interruptCallBack == NULL) {
    interruptCallBack = dummy;
  }

  /* Enable and set EXTI Line0 Interrupt to the lowest priority */
  nviccfg.NVIC_IRQChannel = NVIC_IRQ;
  nviccfg.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  nviccfg.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  nviccfg.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nviccfg);
}

void hw_exti0_attachISR(void (*interrupt)(void)) {
  interruptCallBack = interrupt;
}

int32_t hw_exti0_time(void) {
  return diffTime;
}

float hw_exti0_error(void) {
  float difference;
  difference = 1.0/((float)diffTime);
  difference -= 1e-6;
  if (difference < 0) {
    difference = -difference;
  }
  return difference;
}

#include "interrupt.h"

void EXTI9_5_IRQHandler(void) {
  if (EXTI_GetITStatus(EXTI_LINE) != RESET){
    diffTime = hw_timer2_reset();
    hw_adc_reset();
    hw_timer2_external();
    diffTimeTrigger = 1;
    interruptCallBack();
    EXTI_ClearITPendingBit(EXTI_LINE);
  }
}
