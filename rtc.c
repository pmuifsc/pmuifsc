
#include "rtc.h"

#include "hw_timer2.h"
#include "hw_exti0.h"

static uint8_t  UTCLocked;
static volatile uint32_t SOC;
static uint32_t SOCsync, SOCref;

void rtc_maintenance(void) {
  SOC++;
}

void rtc_setup(void)
{
  SOC = 0;
  SOCref = 1451606400; // 01/01/2016 00:00:00
  SOCsync = 0;
  UTCLocked = 1;
  // attach tick callback function
  hw_exti0_attachISR(rtc_maintenance);
  hw_timer2_attachISR(rtc_maintenance);
}

static inline uint64_t compute64(void) {
  uint32_t SOC1, SOC2, fraqSec;
  uint64_t ms64;
  SOC1 = SOC;
  fraqSec = hw_timer2_read() / (TIMER2_BASE/1000);
  SOC2 = SOC;
  if (SOC1 != SOC2) {
    if (fraqSec < 500) {
      SOC1 = SOC2;
    }
  }
  ms64 = SOC1;
  ms64 *= 1000;
  ms64 += fraqSec;
  return ms64;
}

uint32_t rtc_get24(void) {
  return ((uint32_t)compute64()) & 0x00FFFFFF;
}

uint32_t rtc_get32(void) {
  return (uint32_t)compute64();
}

uint64_t rtc_get64(void) {
  return compute64();
}

uint32_t rtc_get_ms(void) {
  return rtc_get32();
}

uint64_t rtc_get_ms64(void) {
  return rtc_get64();
}

void delay_ms(uint16_t miliseconds) {
  uint32_t time = rtc_get32();
  uint32_t trigger = time + miliseconds;
  do {
    time = rtc_get32();
  } while (time > trigger);
  do {
    time = rtc_get32();
  } while (time < trigger);
}

void rtc_setSOC(uint32_t _SOC) {
  SOCref = _SOC - SOC;
  return;
}

uint32_t rtc_getSOC(void) {
  return SOC + SOCref;
}

uint32_t rtc_getSOCRef(void) {
  return SOCref;
}

void rtc_syncSOC(void) {
  SOCsync = SOC;
  return;
}

uint32_t rtc_getLastSync(void) {
  return SOC - SOCsync;
}

void rtc_lockUTC(void) {
  rtc_syncSOC();
  UTCLocked = 1;
}

void rtc_unlockUTC(void) {
  UTCLocked = 0;
}

uint8_t rtc_lockedUTC(void) {
  return UTCLocked;
}
