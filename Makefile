
# Put your stlink folder here so make burn will work.
STLINK=Tools

# Binaries will be generated with this name (.elf, .bin, .hex, etc)
PROJ_NAME=pmu

ROOT=.
include Makefile.inc

# Normally you shouldn't need to change anything below this line!
#######################################################################################

# Paths, prefixes and suffixes
OBJDIR := Object
LIBDIR := Lib
INCDIR := . TM stm32f4xx lwip lwip/ipv4 lwip/ipv6 ethernet ethernet/arch
INCDIR := $(foreach inc,$(INCDIR),Include/$(inc))
SRCDIRS = . ethernet ethernet/arch
#lwIP compilation
LWIPDIR = lwip


ifeq ($(USE_FATFS),YES)
	SRCDIRS+=fatfs
	CFLAGS+=-DUSE_FATFS
	ifeq ($(FATFS_TESTMODE),YES)
		SRCDIRS+=fatfs/example
		CFLAGS+=-DFATFS_TESTMODE
	endif
endif

ifeq ($(PART),STM32F429ZI)
	SRCDIRS += ethernet/STM32F4x7
else ifeq ($(PART),STM32F407VG)
	SRCDIRS += ethernet/enc28j60
	CFLAGS += -DENC28J60_USE_PBUF
endif

PREFIX=arm-none-eabi-
CC := $(PREFIX)gcc
LD := $(PREFIX)gcc
AR := $(PREFIX)ar
AS := $(PREFIX)as
SIZE := $(PREFIX)size
OBJCOPY := $(PREFIX)objcopy

# Define Flags to compile and link

CFLAGS += -ggdb
CFLAGS += -T $(shell echo $(PART) | tr '[:upper:]' '[:lower:]').ld
CFLAGS += $(foreach inc,$(INCDIR),-I$(inc))
CFLAGS += -Werror=implicit-function-declaration
CFLAGS += -DHSE_VALUE=8000000 -DPLL_M=8
# CFLAGS += -Wextra -Werror
# CFLAGS += -DUSE_USB_OTG_FS

ASFLAGS = -mthumb-interwork $(COREFLAGS)

LDFLAGS  = -O3 -static -Wl,--gc-sections
LDFLAGS += -L. -lgcc -lc -lnosys -lm
LDFLAGS += -Wl,--start-group  $(LIBS) -Wl,--end-group

VPATH += $(SRCDIRS)
# Put your source files here (*.c *.s)
SRCS:= $(foreach src,$(SRCDIRS),$(shell find $(src) -maxdepth 1 -name "*.[c]" -printf "%f "))
SRCS+= startup_$(shell echo $(PART) | tr '[:upper:]' '[:lower:]').s

# add Libraries and objects 
OBJS := $(subst .c,.o,$(SRCS))
OBJS := $(subst .s,.o,$(OBJS))
LIBS  = libperiph_stm32f4.a libdsplib_lm4f.a liblwip.a
ifeq ($(PART),STM32F429ZI)
	LIBS += libeth_stm32f4.a
endif
LIBS := $(foreach lib,$(LIBS),$(LIBDIR)/$(lib))

.PHONY: all gdb size

all: $(PROJ_NAME).bin

#not working
gdb: $(PROJ_NAME).elf burn
#	sudo $(STLINK)/st-util &
	@echo Not working
#	arm-none-eabi-gdb $(PROJ_NAME).elf
#	nemiver
#	sudo killall st-util

$(PROJ_NAME).bin: $(PROJ_NAME).elf

size: $(PROJ_NAME).elf
	@$(SIZE) -A --radix=16 $<

$(PROJ_NAME).elf: $(foreach obj,$(OBJS),$(OBJDIR)/$(obj)) $(foreach lib,$(LIBS),$(lib))
	$(LD) $(CFLAGS) $^ $(LDFLAGS) -o $@
	@$(SIZE) -A --radix=16 $@

$(LIBDIR)/libdsplib_lm4f.a: STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Makefile | $(LIBDIR)
	(cd STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/ && make)
	
$(LIBDIR)/libperiph_stm32f4.a: STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/STM32F4xx_StdPeriph_Driver/Makefile | $(LIBDIR)
	(cd STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/STM32F4xx_StdPeriph_Driver/ && make)

$(LIBDIR)/libarm_cortexM4lf_math.a: STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Lib/GCC/libarm_cortexM4lf_math.a | $(LIBDIR)
	cp -p $< $@

$(LIBDIR)/libeth_stm32f4.a: STM32F4x7_ETH_LwIP_V1.1.1/Libraries/STM32F4x7_ETH_Driver/src/Makefile Include/ethernet/stm32f4x7_eth_conf.h | $(LIBDIR)
	(cd STM32F4x7_ETH_LwIP_V1.1.1/Libraries/STM32F4x7_ETH_Driver/src/ && make)

# Creates destination directory if it doesn't exist
$(OBJDIR):
	@mkdir -p $(OBJDIR)

$(LIBDIR):
	@mkdir -p $(LIBDIR)

$(LWIPDIR):
	@git clone git://git.savannah.nongnu.org/lwip.git -b DEVEL-1_4_1

$(OBJDIR)/%.o: %.c Include/*.h Makefile | $(OBJDIR) $(LWIPDIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/%.o: %.s | $(OBJDIR) $(LWIPDIR)
	$(AS) $(ASFLAGS) -o $@ $<

%.bin: %.elf
	$(OBJCOPY) -O binary $< $@

%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

dump: $(PROJ_NAME).elf
	arm-none-eabi-objdump -D $< > $@
	less $@
	rm $@

.PHONY: veryclean clean
veryclean: clean
	rm -f $(LIBDIR)/*
	rm -rf $(LIBDIR) $(OBJDIR)

clean:
	rm -f $(OBJDIR)/*.o $(PROJ_NAME).elf $(PROJ_NAME).hex $(PROJ_NAME).bin
	rm -f $(OBJDIR)/$(LWIPDIR)/*.o $(LIBDIR)/liblwip.a
	(cd STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/STM32F4xx_StdPeriph_Driver/ && make clean)
	(cd STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/ && make clean)

allow_user:
	@echo 'SUBSYSTEMS=="usb", ATTR{idVendor}=="0483", ATTR{idProduct}=="3748", ACTION=="add", GROUP="dialout", MODE="0664"' > tmp.rules
	@echo 'SUBSYSTEMS=="usb", ATTR{idVendor}=="0483", ATTR{idProduct}=="374b", ACTION=="add", GROUP="dialout", MODE="0664"' >> tmp.rules
	@sudo mv tmp.rules /lib/udev/rules.d/90-STLINK.rules
	@echo "Allowed. Unplug and re-plug usb devive. Enjoy"

# Flash the STM32F4
burn: $(STLINK)/st-flash $(PROJ_NAME).bin
	$(STLINK)/st-flash write $(PROJ_NAME).bin 0x8000000
	$(STLINK)/st-flash reset

sudoburn: $(STLINK)/st-flash $(PROJ_NAME).bin
	sudo $(STLINK)/st-flash write $(PROJ_NAME).bin 0x8000000
	sudo $(STLINK)/st-flash reset

#Build stlink programmer
$(STLINK)/st-flash:
	@(cd $(STLINK) && make)

#lwIP compilation
# use this with lwip 1.5 development or later
#LWIPOBJS = etharp.o mem.o memp.o netif.o pbuf.o raw.o stats.o sys.o tcp.o tcp_in.o tcp_out.o udp.o dhcp.o init.o def.o timers.o dns.o inet_chksum.o err.o icmp.o ip_frag.o ip4_addr.o ip4.o
# use this with lwip 1.4.1
LWIPOBJS = etharp.o mem.o memp.o netif.o pbuf.o raw.o stats.o sys.o tcp.o tcp_in.o tcp_out.o udp.o dhcp.o init.o def.o timers.o dns.o inet_chksum.o err.o icmp.o ip_frag.o ip_addr.o ip.o

LWIPCFLAGS = -I./lwip/src/include/ipv4 -I./lwip/src/include/ipv6 -I./lwip/src/include -IInclude/lwip
vpath %.c lwip/src/netif lwip/src/core lwip/src/api lwip/src/core/ipv4

$(LIBDIR)/liblwip.a: $(foreach obj,$(LWIPOBJS),$(OBJDIR)/$(LWIPDIR)/$(obj)) Include/$(LWIPDIR)/lwipopts.h | $(LWIPDIR) $(LIBDIR)
	$(AR) rcs $@ $^

$(OBJDIR)/$(LWIPDIR)/%.o: %.c Include/lwip/lwipopts.h Include/ethernet/arch/*.h | $(LWIPDIR) $(OBJDIR)/$(LWIPDIR)
	$(CC) $(CFLAGS) $(LWIPCFLAGS) -c $< -o $@

$(OBJDIR)/$(LWIPDIR)/%.o: %.s Include/lwip/lwipopts.h | $(LWIPDIR) $(OBJDIR)/$(LWIPDIR)
	$(AS) $(ASFLAGS) -o $@ $<

$(OBJDIR)/$(LWIPDIR): | $(OBJDIR)
	@mkdir -p $@
