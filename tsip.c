
#include "tsip.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "rtc.h"
#include "hw_uart2.h"
#include "hw_timer2.h"
#include "calculus.h"

#include "core/arm_math.h"
#include "core/arm_common_tables.h"

struct __attribute__((packed)) TSIP_8F_AB {
  uint32_t secondsOfWeek;
  uint16_t weekNumber;
  int16_t UTC_offset;
  uint8_t GPS_UTC_time : 1;
  uint8_t GPS_UTC_PPS  : 1;
  uint8_t time_set     : 1;
  uint8_t has_UTC      : 1;
  uint8_t from_UTC     : 1;
  uint8_t seconds;
  uint8_t minutes;
  uint8_t hours;
  uint8_t day;
  uint8_t month;
  uint16_t year;
} tsip_ab;

#define TSIP_AC_ALARMS_ANTENNA_OPEN           (1<<1)
#define TSIP_AC_ALARMS_ANTENNA_SHORT          (1<<2)
#define TSIP_AC_ALARMS_NOT_TRACKING           (1<<3)
#define TSIP_AC_ALARMS_SURVEY_IN              (1<<5)
#define TSIP_AC_ALARMS_NO_STORED_POS          (1<<6)
#define TSIP_AC_ALARMS_LEAP_PENDING           (1<<7)
#define TSIP_AC_ALARMS_TEST_MODE              (1<<8)
#define TSIP_AC_ALARMS_POS_QUESTIONABLE       (1<<9)
#define TSIP_AC_ALARMS_ALMANAC_NOT_COMPLETE   (1<<11)
#define TSIP_AC_ALARMS_PPS_NOT_GENERATED      (1<<12)

struct __attribute__((packed)) TSIP_8F_AC {
  uint8_t recvMode;
  uint8_t reserved0;
  uint8_t SelfSurveyProgress;
  uint32_t reserved1;
  uint16_t reserved2;
  uint16_t alarms;
  uint8_t GPSStatus;
  uint8_t reserved3;
  uint8_t SpareStatus1;
  uint8_t SpareStatus2;
  float32_t LocalClockBias;
  float32_t LocalClockBiasRate;
  uint32_t reserved4;
  float32_t reserved5;
  float32_t temperature;
  float64_t latitude;
  float64_t longitude;
  float64_t altitude;
  float32_t PPS_ERROR;
  uint32_t reserved6;
} tsip_ac;

static uint32_t getTimeStamp(void);
static void setGPSStatus(void);
static uint8_t readTSIP(uint8_t *packId, uint8_t *packData);

#define SWAP_DATA_64(data)    data = (data & 0x00000000FFFFFFFF) << 32 | (data & 0xFFFFFFFF00000000) >> 32; \
                              data = (data & 0x0000FFFF0000FFFF) << 16 | (data & 0xFFFF0000FFFF0000) >> 16; \
                              data = (data & 0x00FF00FF00FF00FF) << 8  | (data & 0xFF00FF00FF00FF00) >> 8
#define SWAP_DATA_32(data)    data = (data & 0x0000FFFF) << 16 | (data & 0xFFFF0000) >> 16; \
                              data = (data & 0x00FF00FF) << 8  | (data & 0xFF00FF00) >> 8
#define SWAP_DATA_16(data)    data = (data & 0x00FF) << 8  | (data & 0xFF00) >> 8

void tsipInit() {
  hw_uart2_Init(); // Initialize uart for receive GPS data or debug
}


void tsipPrint(char *buffer) {
  int8_t okok = tsipPool();
  sprintf(buffer, "%d:%d:%d %d/%d/%d\n",
          tsip_ab.hours,
          tsip_ab.minutes,
          tsip_ab.seconds,
          tsip_ab.day,
          tsip_ab.month,
          tsip_ab.year
         );
}

static uint32_t getTimeStamp(void) {
  uint32_t timeStamp = 315964800; // Seconds from 1/1/1970 to 6/1/1980
  timeStamp += tsip_ab.weekNumber * (7 * 24 * 60 * 60); // week seconds
  timeStamp += tsip_ab.secondsOfWeek;
  timeStamp -= tsip_ab.UTC_offset;
  return timeStamp;
  }

static void setGPSStatus(void) {
  static uint16_t alarmsChanged = -1;
  if (alarmsChanged != tsip_ac.alarms) {
    alarmsChanged = tsip_ac.alarms;
    if (alarmsChanged & TSIP_AC_ALARMS_PPS_NOT_GENERATED) {
      hw_timer2_interrupt();
    } else {
      hw_timer2_external();
    }
    if (alarmsChanged & TSIP_AC_ALARMS_LEAP_PENDING) {
      // treat leap second
    }
  }
  if (tsip_ac.GPSStatus == 0) {
    rtc_lockUTC();
  } else {
    rtc_unlockUTC();
  }
}

#define MAXPACK_DATA 100
uint8_t tsipPool() {
  uint8_t packId = 0, packIndex = 0;
  static uint8_t packData[MAXPACK_DATA];
  while (1) {
    packIndex = readTSIP(&packId, packData);
    if (packIndex == (uint8_t)-1) {
      // Serial buffer empty (no more packets avaliable)
      return -2;
    }
    if (packIndex == -2) {
      // Error reading packet
      return -1;
    }
    if (packIndex == 0) {
      // Error empty packet
      return -1;
    }
    if (packId == 0x8F) {
      if (packData[0] == 0xAB) {
        memcpy(&tsip_ab.secondsOfWeek, &packData[1], packIndex - 1);
        SWAP_DATA_32(tsip_ab.secondsOfWeek);
        SWAP_DATA_16(tsip_ab.weekNumber);
        SWAP_DATA_16(tsip_ab.UTC_offset);
        SWAP_DATA_16(tsip_ab.year);
        rtc_setSOC(getTimeStamp());
      }
      if (packData[0] == 0xAC) {
        memcpy(&tsip_ac.recvMode, &packData[1], packIndex - 1);
        SWAP_DATA_16(tsip_ac.alarms);
        SWAP_DATA_32(*((uint32_t *)&tsip_ac.LocalClockBias));
        SWAP_DATA_32(*((uint32_t *)&tsip_ac.LocalClockBiasRate));
        SWAP_DATA_32(*((uint32_t *)&tsip_ac.temperature));
        SWAP_DATA_64(*((uint64_t *)&tsip_ac.latitude));
        SWAP_DATA_64(*((uint64_t *)&tsip_ac.longitude));
        SWAP_DATA_64(*((uint64_t *)&tsip_ac.altitude));
        SWAP_DATA_32(*((uint32_t *)&tsip_ac.PPS_ERROR));
        setGPSStatus();
      }
    }
  }
}

static uint8_t readTSIP(uint8_t *packId, uint8_t *packData) {
  uint8_t tmpByte;
  static uint8_t status = 0, packIndex = 0, recvId = 0;
  /* status :
   * 0 -> Waiting new packet
   * 1 -> Get packId
   * 2 -> Receiving normal data
   * 3 -> Received first 0x10 (control byte)
   */

  while (hw_uart2_read((char *)&tmpByte) != (uint8_t)-1) {
    if (tmpByte == 0x10) { // Treat 0x10
      switch (status) {
        case 0: // No data reading (begin of pack)
          packIndex = 0;
          status = 1;
          break;
        case 1: // Error in sync message
          status = 0;
          packIndex = 0;
          return -2;
        case 2: // one 0x10 in datapack (check if is 0x10 data or End Of Pack)
          status = 3;
          break;
        case 3: // two consecutive 0x10 control are a single 0x10 data
if (packIndex >= MAXPACK_DATA) {
  status = 0;
  return -2;
}
          packData[packIndex++] = tmpByte;
          status = 2;
          break;
      }
      continue;
    }
    if (status == 0) { // loops wainting for 0x10
      packIndex = 0;
      continue;
    }
    if (status == 1) {
      recvId = tmpByte;
      status = 2;
      continue;
    }
    if (status == 2) {
if (packIndex >= MAXPACK_DATA) {
  status = 0;
  return -2;
}
      packData[packIndex++] = tmpByte;
      continue;
    }
    if (status == 3) {
      // End of Packet
      status = 0;
      if (tmpByte == 0x03) {
        *packId = recvId;
        return packIndex;
      }
      packIndex = 0;
      // Unknow control byte
      return -2;
    }
  }
  return -1;
}
