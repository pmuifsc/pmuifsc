
#include "hw_clock.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_rcc.h"
#include "stm32f4xx/misc.h"

#include <stdio.h>

// Initialize general clocks
void hw_clock_Init(void) {
  // Enable the clock to ALL GPIO Peripheral, to grantee the correct execution
  //  of others peripherals
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CCMDATARAMEN, ENABLE);
  
#ifdef STM32F429_439xx
  // adjust timers clock
  RCC->DCKCFGR |= RCC_DCKCFGR_TIMPRE;
#endif

  // Stall instruction pipeline, until instruction completes, as
  //  per Errata 2.1.13, "Delay after an RCC peripheral clock enabling
  __asm("dsb");
  return;
}
