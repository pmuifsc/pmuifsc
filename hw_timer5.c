
#include "hw_timer5.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_tim.h"
#include "stm32f4xx/stm32f4xx_rcc.h"
#include "stm32f4xx/misc.h"

#include <stdarg.h>
#include <stdio.h>

// 50 is default value
#define NVIC_PRIORITY 14

#include "hw_uart1.h"

static void dummy(void) {
  return;
}

static void (*interruptCallBack)(void) = NULL;

void hw_timer5_Init() {
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

  // Configure to full scale
  TIM_TimeBaseStructInit(&TIM_TimeBaseInitStruct);

  // Devices has different source clock, TIMPRE set in hw_clock.c
#ifdef STM32F40_41xxx
  TIM_TimeBaseInitStruct.TIM_Prescaler = ((SystemCoreClock/2) / 12000000)-1;
#elif STM32F429_439xx
  TIM_TimeBaseInitStruct.TIM_Prescaler = ((SystemCoreClock) / 12000000)-1;
#endif
  TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period = 12000; // 0xFFFFFFFF; // Maximun
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x0000;

  TIM_InternalClockConfig(TIM5);

  TIM_TimeBaseInit(TIM5, &TIM_TimeBaseInitStruct);
  TIM_SetCounter(TIM5, 0);
#if 1
  // define a dummy callback function
  if (interruptCallBack == NULL) {
    interruptCallBack = dummy;
  }

  NVIC_InitTypeDef nviccfg;
  // Configure Interrupt priority in NVIC
  nviccfg.NVIC_IRQChannel = TIM5_IRQn;
  nviccfg.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  nviccfg.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  nviccfg.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nviccfg);

  TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);
  TIM_Cmd(TIM5, ENABLE);

#endif
}

void hw_timer5_start() {
  TIM_Cmd(TIM5, ENABLE);
}

void hw_timer5_restart() {
  TIM_SetCounter(TIM5, 0);
  TIM_Cmd(TIM5, ENABLE);
}

uint32_t hw_timer5_reset() {
  uint32_t tmp = TIM_GetCounter(TIM5);
  TIM_SetCounter(TIM5, 0);
  return tmp;
}

void hw_timer5_stop() {
  TIM_Cmd(TIM5, DISABLE);
}

uint32_t hw_timer5_read() {
  return TIM_GetCounter(TIM5);
}

void hw_timer5_attachISR(void (*interrupt)(void)) {
  interruptCallBack = interrupt;
}

#include "interrupt.h"

void TIM5_IRQHandler(void) {
  interruptCallBack();
  TIM_ClearFlag(TIM5, TIM_FLAG_Update);
}
