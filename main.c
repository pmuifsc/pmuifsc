
#include <stdio.h>

#include "Connection.h"
#include "calculus.h"
#include "defines.h"

#include "tsip.h"
#include "Synchrophasor.h"

#include "hw_clock.h"
#include "hw_timer2.h"
#include "hw_timer5.h"
#include "hw_uart1.h"
#include "hw_adc.h"
#include "hw_exti0.h"
#include "hw_uid.h"
#include "rtc.h"
#include "interrupt.h"
#include "debug.h"
#include "watchdog.h"

#include "system_stm32f4xx.h"

#include "stm32f4xx.h"

#include <math.h>

char threePhasorsCalc(uint16_t **waves, uint16_t samples, uint8_t nWaves);
void sendToServer();

char __buff[500];
int main(void)
{
  uint8_t usedBuffer;
  uint8_t waveNum[2];
  uint16_t *waves[2];
  uint8_t sendData;
  uint8_t syncRun = 0;

  SystemCoreClockUpdate();
  hw_clock_Init();
  hw_uart1_Init(); // Initialize uart for debug
  hw_exti0_Init(); // Initialize Ext Int, that trigger timers for sync
  hw_timer2_Init(); // Initialize timer 2
  hw_timer2_start(); // start counting
  rtc_setup();
  delay_ms(2);
  hw_adc_Init(F_FREQ);  // Initialize ADC and DMA
  tsipInit(); // Start TSIP

#ifdef USE_FATFS
  hw_timer5_Init();
  extern void disk_timerproc(void);
  hw_timer5_attachISR(disk_timerproc);
  hw_timer5_start();
#ifdef FATFS_TESTMODE
  extern int sdtest(void);
  sdtest();
#endif
#endif

  connectionInit();

  usedBuffer = lastUsedBuffer;

            int8_t ch;
            GPIO_InitTypeDef gpiocfg;
            gpiocfg.GPIO_Pin = GPIO_Pin_1;
            gpiocfg.GPIO_Mode = GPIO_Mode_OUT;
            gpiocfg.GPIO_Speed = GPIO_Speed_100MHz;
            gpiocfg.GPIO_OType = GPIO_OType_PP;
            gpiocfg.GPIO_PuPd = GPIO_PuPd_NOPULL;
            GPIO_Init(GPIOB, &gpiocfg);

            GPIO_WriteBit(GPIOB, GPIO_Pin_1, 0);


  uart1Print("PMU IFSC v0.2 Running!!\n");
  uart1Print(__TIMESTAMP__);
  uart1Print("\n");

  InitWatchdog();
  while(1) {

    hw_uart1_read((char *) &ch);
    if (ch) {
      switch(ch) {
        case '0':
          sprintf(__buff, "%d\n", syncRun);
          break;
        case '1':
          sprintf(__buff, "%d\n", samplesSynced);
          break;
        case '2':
          sprintf(__buff, "%d\n", lastUsedBuffer);
          break;
        case '3':
          sprintf(__buff, "0x%08X\n", *((uint32_t *)0x40026400));
          break;
        case '4':
          sprintf(__buff, "0x%08X\n", *((uint32_t *)0x40026404));
          break;
        case '5':
          sprintf(__buff, "0x%08X\n", *((uint32_t *)0x40010424));
          break;
        default:
          sprintf(__buff, "%c", ch);
          break;
      }
      uart1Print(__buff);
    }
    if (samplesSynced && !syncRun) {
      syncRun = 1;
    } else if (!samplesSynced && syncRun) {
      syncRun = 0;
    }
    if ((usedBuffer != lastUsedBuffer) && syncRun) {
      uint16_t i;
      static PowerLineInfo res;
      usedBuffer = lastUsedBuffer;
      switch(lastUsedBuffer & 0x03) {
        case 0:
          waveNum[0] = 1;
          waveNum[1] = 2;
          break;
        case 1:
          waveNum[0] = 2;
          waveNum[1] = 3;
          break;
        case 2:
          waveNum[0] = 3;
          waveNum[1] = 0;
          break;
        case 3:
          waveNum[0] = 0;
          waveNum[1] = 1;
          break;
      }
      dirtyBuffer[waveNum[0]] = 0;
      dirtyBuffer[waveNum[1]] = 0;

      {
        static int16_t avg0[NSAMPLES*3], avg1[NSAMPLES*3];
        int i, j, k;
        int32_t tmp0, tmp1;
        // Pessimo para cache
        for (i = 0; i < NSAMPLES; i++) {
          for (j = 0; j < 3; j++) {
            tmp0 = 0;
            tmp1 = 0;
            for (k = 0; k < COUNTSAMPLES/NSAMPLES; k++) {
              tmp0 += ((int16_t *)adcBuffer[waveNum[0]])[(i*3*8)+j+(k*3)];
              tmp1 += ((int16_t *)adcBuffer[waveNum[1]])[(i*3*8)+j+(k*3)];
            }
            avg0[i*3+j] = tmp0 / (COUNTSAMPLES/NSAMPLES);
            avg1[i*3+j] = tmp1 / (COUNTSAMPLES/NSAMPLES);
          }
        }
//         static uint16_t waveNumTestAVG[2][768];
        GPIO_WriteBit(GPIOB, GPIO_Pin_1, 1);
//         averageAVG((uint16_t *)adcBuffer[waveNum[0]], waveNumTestAVG[0]);
//         averageAVG((uint16_t *)adcBuffer[waveNum[1]], waveNumTestAVG[1]);
        res.timestamp= bufferTimeStamp[waveNum[1]][STAMP_SOC];
        res.fracsec = bufferTimeStamp[waveNum[1]][STAMP_FRAC];
        ch = powerLineEvaluate(avg0, avg1, &res);
//         ch = -1;
//         tsipPrint(__buff);
//         uart1Print(__buff);
        GPIO_WriteBit(GPIOB, GPIO_Pin_1, 0);
//         continue;
      }
      if (dirtyBuffer[waveNum[0]] == 1 || dirtyBuffer[waveNum[1]] == 1) {

          sprintf(__buff,"Deadline Lost --> %d.%07d\n",
                  bufferTimeStamp[waveNum[1]][STAMP_SOC], (uint32_t)(bufferTimeStamp[waveNum[1]][STAMP_FRAC] / 12.0));
          uart1Print(__buff);
      }
      else if (syncRun > 10) {
        if (1) {
          switch (ch) {
            case -1:
              sprintf(__buff,"No power\n\t");
              break;
            case 0:
              sprintf(__buff,"Phase A\t");
              break;
            case 1:
              sprintf(__buff,"Phase B\t");
              break;
            case 2:
              sprintf(__buff,"Phase C\t");
              break;
          }
          uart1Print(__buff);
          sprintf(__buff,"%f\t-->\t%f\t%f\t%d,%d,%d.%07d\n", res.freq,
                  (1.0/*/1.41421356*/) * sqrt(res.phasor[ch].real*res.phasor[ch].real+res.phasor[ch].imag*res.phasor[ch].imag), rad2deg(angle(res.phasor[ch])), hw_exti0_time(), hw_timer2_opStatus(),
                  bufferTimeStamp[waveNum[1]][STAMP_SOC], (uint32_t)(bufferTimeStamp[waveNum[1]][STAMP_FRAC] / 12.0));
          uart1Print(__buff);
          if (diffTimeTrigger == 1) {
            uart1Print("PPS\n");
            diffTimeTrigger = 0;
          }
        }
        uint16_t len;
        uint8_t *buff;
        static uint32_t configCount = 3599;
        if (configCount >= 3599) {
          // send config data
          buff = getConfigurationFrame(&len);
          connectionSendDataAll(buff, len);
          configCount = 0;
        }
        configCount++;
        buff = getDataFrame(&len, &res);
        connectionSendDataAll(buff, len);
      } else {
        syncRun++;
      }
    }
    tsipPool();
    connectionPool();
    FeedDog();
  }

#if 0
  while(1) {
    // RTOS: adcPeripheral sends message to a task that calculates phasor
    if (usedBuffer != lastUsedBuffer) {
      usedBuffer = lastUsedBuffer;
      // check which buffer will use
      switch(usedBuffer & 0x03) {
        case 0:
          waveNum[0] = 1;
          waveNum[1] = 2;
          break;
        case 1:
          waveNum[0] = 2;
          waveNum[1] = 3;
          break;
        case 2:
          waveNum[0] = 3;
          waveNum[1] = 0;
          break;
        case 3:
          waveNum[0] = 0;
          waveNum[1] = 1;
          break;
      }
      // setup buffer
      waves[0] = (uint16_t *)adcBuffer[waveNum[0]];
      waves[1] = (uint16_t *)adcBuffer[waveNum[1]];
      // check if buffer are not busy (by ADC peripheral)
      if (!(dirtyBuffer[waveNum[0]] || dirtyBuffer[waveNum[1]])) {
        // calculate phasors must setup return value
        // the waves, number of samples to each phase, number of waves
        threePhasorsCalc(waves, NSAMPLES, 2);
        // RTOS: adc sends a message indicating that buffer was modified
        //and invalidate calculus
        // validate if the buffrers still idle
        if (!(dirtyBuffer[waveNum[0]] || dirtyBuffer[waveNum[1]])) {
          // RTOS: send a message to task to send to ethernet server
          sendData = 1;
        }
      }
    }

    // Must read serial and analyse from GPS / RTOS: Could be a task todo this

    // Must read another serial to set the user configurable parameters
    // like number of samples, powerLineFrequency, and another parametes

    if (sendData) {
      sendData = 0;
      sendToServer();
      // save in backup memory
    }

    // Execute Connection pool, DHCP, DNS, incomming Packets
  }
#endif
}

char threePhasorsCalc(uint16_t **waves, uint16_t samples, uint8_t nWaves) {
  // Not implemented yet
  // waves come in format waves[x][y], which:
  // x < nWaves, lesser x are older samples set
  // y < (samples * 3), lesser y are firsts samples which:
  // (y % 3) = 0 is phase A
  // (y % 3) = 1 is phase B
  // (y % 3) = 2 is phase C]
  return 0;
}

void sendToServer() {
  // Not implemented yet
}

static void unexpected_interrupt_handler(char *irq) {
  char c;
  uart1Print("\n");
  uart1Print(irq);
  uart1Print("\n");
  char state = 0;
  while(1) {
    USART1_IRQHandler();
    hw_uart1_read(&c);
    if (c == 'O') break;
  }
}

#define HANDLER(irq) \
void irq(void) { \
  unexpected_interrupt_handler(#irq); \
} \


// HANDLER(Reset_Handler)
HANDLER(NMI_Handler)
HANDLER(HardFault_Handler)
HANDLER(MemManage_Handler)
HANDLER(BusFault_Handler)
HANDLER(UsageFault_Handler)
HANDLER(SVC_Handler)
HANDLER(DebugMon_Handler)
HANDLER(PendSV_Handler)
HANDLER(SysTick_Handler)

HANDLER(WWDG_IRQHandler)
HANDLER(PVD_IRQHandler)
HANDLER(TAMP_STAMP_IRQHandler)
HANDLER(RTC_WKUP_IRQHandler)
HANDLER(FLASH_IRQHandler)
HANDLER(RCC_IRQHandler)
HANDLER(EXTI0_IRQHandler)
HANDLER(EXTI1_IRQHandler)
HANDLER(EXTI2_IRQHandler)
HANDLER(EXTI3_IRQHandler)
HANDLER(EXTI4_IRQHandler)
HANDLER(DMA1_Stream0_IRQHandler)
HANDLER(DMA1_Stream1_IRQHandler)
HANDLER(DMA1_Stream2_IRQHandler)
HANDLER(DMA1_Stream3_IRQHandler)
HANDLER(DMA1_Stream4_IRQHandler)
HANDLER(DMA1_Stream5_IRQHandler)
HANDLER(DMA1_Stream6_IRQHandler)
HANDLER(ADC_IRQHandler)
HANDLER(CAN1_TX_IRQHandler)
HANDLER(CAN1_RX0_IRQHandler)
HANDLER(CAN1_RX1_IRQHandler)
HANDLER(CAN1_SCE_IRQHandler)
// HANDLER(EXTI9_5_IRQHandler)
HANDLER(TIM1_BRK_TIM9_IRQHandler)
HANDLER(TIM1_UP_TIM10_IRQHandler)
HANDLER(TIM1_TRG_COM_TIM11_IRQHandler)
HANDLER(TIM1_CC_IRQHandler)
// HANDLER(TIM2_IRQHandler)
HANDLER(TIM3_IRQHandler)
HANDLER(TIM4_IRQHandler)
HANDLER(I2C1_EV_IRQHandler)
HANDLER(I2C1_ER_IRQHandler)
HANDLER(I2C2_EV_IRQHandler)
HANDLER(I2C2_ER_IRQHandler)
HANDLER(SPI1_IRQHandler)
HANDLER(SPI2_IRQHandler)
// HANDLER(USART1_IRQHandler)
// HANDLER(USART2_IRQHandler)
// HANDLER(USART3_IRQHandler)
HANDLER(EXTI15_10_IRQHandler)
HANDLER(RTC_Alarm_IRQHandler)
HANDLER(OTG_FS_WKUP_IRQHandler)
HANDLER(TIM8_BRK_TIM12_IRQHandler)
// HANDLER(TIM8_UP_TIM13_IRQHandler)
HANDLER(TIM8_TRG_COM_TIM14_IRQHandler)
HANDLER(TIM8_CC_IRQHandler)
HANDLER(DMA1_Stream7_IRQHandler)
HANDLER(FSMC_IRQHandler)
HANDLER(SDIO_IRQHandler)
// HANDLER(TIM5_IRQHandler)
HANDLER(SPI3_IRQHandler)
HANDLER(UART4_IRQHandler)
HANDLER(UART5_IRQHandler)
HANDLER(TIM6_DAC_IRQHandler)
HANDLER(TIM7_IRQHandler)
HANDLER(DMA2_Stream0_IRQHandler)
HANDLER(DMA2_Stream1_IRQHandler)
HANDLER(DMA2_Stream2_IRQHandler)
HANDLER(DMA2_Stream3_IRQHandler)
// HANDLER(DMA2_Stream4_IRQHandler)
HANDLER(ETH_IRQHandler)
HANDLER(ETH_WKUP_IRQHandler)
HANDLER(CAN2_TX_IRQHandler)
HANDLER(CAN2_RX0_IRQHandler)
HANDLER(CAN2_RX1_IRQHandler)
HANDLER(CAN2_SCE_IRQHandler)
HANDLER(OTG_FS_IRQHandler)
HANDLER(DMA2_Stream5_IRQHandler)
HANDLER(DMA2_Stream6_IRQHandler)
HANDLER(DMA2_Stream7_IRQHandler)
HANDLER(USART6_IRQHandler)
HANDLER(I2C3_EV_IRQHandler)
HANDLER(I2C3_ER_IRQHandler)
HANDLER(OTG_HS_EP1_OUT_IRQHandler)
HANDLER(OTG_HS_EP1_IN_IRQHandler)
HANDLER(OTG_HS_WKUP_IRQHandler)
HANDLER(OTG_HS_IRQHandler)
HANDLER(DCMI_IRQHandler)
HANDLER(CRYP_IRQHandler)
HANDLER(HASH_RNG_IRQHandler)
HANDLER(FPU_IRQHandler)

HANDLER(UART7_IRQHandler)
HANDLER(UART8_IRQHandler)
HANDLER(SPI4_IRQHandler)
HANDLER(SPI5_IRQHandler)
HANDLER(SPI6_IRQHandler)
HANDLER(SAI1_IRQHandler)
HANDLER(LTDC_IRQHandler)
HANDLER(LTDC_ER_IRQHandler)
HANDLER(DMA2D_IRQHandler)
