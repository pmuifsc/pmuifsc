
#include "hw_timer8.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_tim.h"
#include "stm32f4xx/stm32f4xx_rcc.h"
#include "stm32f4xx/stm32f4xx_gpio.h"
#include "stm32f4xx/misc.h"

#include <stdarg.h>
#include <stdio.h>

#include "calculus.h"
#include "system_stm32f4xx.h"

#include "hw_adc.h"


#define CLOCK_INPUT_PORT  GPIOA
#define CLOCK_INPUT_PIN   GPIO_Pin_0
#define CLOCK_INPUT_PIN_S GPIO_PinSource0

// 44 is default value
#define NVIC_PRIORITY 13

static int16_t timerError;
static uint32_t period;
static uint16_t nsamples;
static uint16_t powerFrequency;
static int32_t sample;
static int32_t periodError;

void hw_timer8_Init(float _powerFrequency, uint16_t _nsamples) {
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

  nsamples = _nsamples;
  powerFrequency = (uint32_t)_powerFrequency;
  period = SystemCoreClock / ((uint32_t)nsamples * powerFrequency * (SystemCoreClock / 12000000));
  timerError = 0;

  // Configure to full scale
  TIM_TimeBaseInitStruct.TIM_Prescaler = (SystemCoreClock / 12000000)-1;
  TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period = period-1;
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x0000;
  TIM_TimeBaseInit(TIM8, &TIM_TimeBaseInitStruct);
  TIM_ARRPreloadConfig(TIM8, ENABLE);
  TIM_UpdateRequestConfig(TIM8, TIM_UpdateSource_Regular);

  // Trigger on upcounter overflow
  TIM_SelectOutputTrigger(TIM8, TIM_TRGOSource_Update);
  TIM_SetCounter(TIM8, 0);

  // Configure Interrupt priority in NVIC, check if is necessary
#if 1
  NVIC_InitTypeDef nviccfg;
  nviccfg.NVIC_IRQChannel = TIM8_UP_TIM13_IRQn;
  nviccfg.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  nviccfg.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  nviccfg.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nviccfg);

  TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE);
//   TIM_Cmd(TIM8, ENABLE);
#endif
}

void hw_timer8_destroy() {
  TIM_DeInit(TIM8);
}

void hw_timer8_start() {
  TIM_Cmd(TIM8, ENABLE);
}

void hw_timer8_restart() {
  // set to mid between samples, less time to reset counter (28 (measured))
  TIM_SetCounter(TIM8, (period/2));
  TIM_Cmd(TIM8, ENABLE);
}

uint32_t hw_timer8_reset() {
  uint32_t tmp = TIM_GetCounter(TIM8);
  uint16_t timValue;
  timValue = period - 1;
  if (timerError > 0) {
    timValue--;
    timerError--;
  } else if (timerError < 0) {
    timValue++;
    timerError++;
  }
  TIM_SetCounter(TIM8, (timValue/2));
  sample++;
  return tmp;
}

void hw_timer8_stop() {
  TIM_Cmd(TIM8, DISABLE);
}

uint32_t hw_timer8_read() {
  return TIM_GetCounter(TIM8);
}

void hw_timer8_set_err(int16_t error) {
  timerError = error;
  periodError = (nsamples * powerFrequency) / timerError;
  sample = 0;
}

// Check if is necessary
#if 1
#include "interrupt.h"

void TIM8_UP_TIM13_IRQHandler(void) {
  uint16_t timValue;
  timValue = period - 1;
  if ((sample % periodError) == 0) {
    if (timerError > 0) {
      timValue--;
      timerError--;
    } else if (timerError < 0) {
      timValue++;
      timerError++;
    }
  }
  TIM_SetAutoreload(TIM8, timValue);
  sample++;
  TIM_ClearFlag(TIM8, TIM_FLAG_Update);
}
#endif
