/**
  ******************************************************************************
  * @file    stm32f4x7_eth_bsp.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    31-July-2013 
  * @brief   STM32F4x7 Ethernet hardware configuration.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <lwip/opt.h>
#include <lwip/netif.h>
#include <lwip/dhcp.h>
#include <stm32f4x7/stm32f4x7_eth.h>
#include <ethernet/ethernetif_stm32f4x7.h>
#include <ethernet/stm32f4x7_eth_bsp.h>
#include <ethernet/stm32f4x7_eth_conf.h>
#include <netconf.h>
#include "debug.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
ETH_InitTypeDef ETH_InitStructure;
extern __IO uint8_t DHCP_state;

/* Private function prototypes -----------------------------------------------*/
static void ETH_GPIO_Config(void);
static void ETH_MACDMA_Config(void);
void ETH_EXTERN_GetSpeedAndDuplex(uint32_t PHYAddress, ETH_InitTypeDef* ETH_InitStruct);

/* Private functions ---------------------------------------------------------*/
#define GET_PHY_LINK_STATUS()		(ETH_ReadPHYRegister(ETHERNET_PHY_ADDRESS, PHY_BSR) & 0x00000004)
// #define GET_PHY_LINK_STATUS()		(ETH_ReadPHYRegister(ETHERNET_PHY_ADDRESS, PHY_SR) & 1)


/**
  * @brief  ETH_BSP_Config
  * @param  None
  * @retval None
  */
void ETH_BSP_Config(void)
{
  /* Configure the GPIO ports for ethernet pins */
  ETH_GPIO_Config();

  /* Configure the Ethernet MAC/DMA */
  ETH_MACDMA_Config();
}

/**
  * @brief  Configures the Ethernet Interface
  * @param  None
  * @retval None
  */
static void ETH_MACDMA_Config(void)
{
  /* Enable ETHERNET clock  */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_ETH_MAC | RCC_AHB1Periph_ETH_MAC_Tx |
                        RCC_AHB1Periph_ETH_MAC_Rx, ENABLE);
                        
  /* Reset ETHERNET on AHB Bus */
  ETH_DeInit();

  /* Software reset */
  ETH_SoftwareReset();

  /* Wait for software reset */
  while (ETH_GetSoftwareResetStatus() == SET);

  /* ETHERNET Configuration --------------------------------------------------*/
  /* Call ETH_StructInit if you don't like to configure all ETH_InitStructure parameter */
  ETH_StructInit(&ETH_InitStructure);

  /* Fill ETH_InitStructure parametrs */
  /*------------------------   MAC   -----------------------------------*/
  ETH_InitStructure.ETH_AutoNegotiation = ETH_AutoNegotiation_Enable;
  ETH_InitStructure.ETH_LoopbackMode = ETH_LoopbackMode_Disable;
  ETH_InitStructure.ETH_RetryTransmission = ETH_RetryTransmission_Disable;
  ETH_InitStructure.ETH_AutomaticPadCRCStrip = ETH_AutomaticPadCRCStrip_Disable;
  ETH_InitStructure.ETH_ReceiveAll = ETH_ReceiveAll_Disable;
  ETH_InitStructure.ETH_BroadcastFramesReception = ETH_BroadcastFramesReception_Enable;
  ETH_InitStructure.ETH_PromiscuousMode = ETH_PromiscuousMode_Disable;
  ETH_InitStructure.ETH_MulticastFramesFilter = ETH_MulticastFramesFilter_Perfect;
  ETH_InitStructure.ETH_UnicastFramesFilter = ETH_UnicastFramesFilter_Perfect;
#ifdef CHECKSUM_BY_HARDWARE
  ETH_InitStructure.ETH_ChecksumOffload = ETH_ChecksumOffload_Enable;
#endif

  /*------------------------   DMA   -----------------------------------*/
  /* When we use the Checksum offload feature, we need to enable the Store and Forward mode: 
  the store and forward guarantee that a whole frame is stored in the FIFO, so the MAC can insert/verify the checksum,
  if the checksum is OK the DMA can handle the frame otherwise the frame is dropped */
  ETH_InitStructure.ETH_DropTCPIPChecksumErrorFrame = ETH_DropTCPIPChecksumErrorFrame_Enable;
  ETH_InitStructure.ETH_ReceiveStoreForward = ETH_ReceiveStoreForward_Enable;
  ETH_InitStructure.ETH_TransmitStoreForward = ETH_TransmitStoreForward_Enable;

  ETH_InitStructure.ETH_ForwardErrorFrames = ETH_ForwardErrorFrames_Disable;
  ETH_InitStructure.ETH_ForwardUndersizedGoodFrames = ETH_ForwardUndersizedGoodFrames_Disable;
  ETH_InitStructure.ETH_SecondFrameOperate = ETH_SecondFrameOperate_Enable;
  ETH_InitStructure.ETH_AddressAlignedBeats = ETH_AddressAlignedBeats_Enable;
  ETH_InitStructure.ETH_FixedBurst = ETH_FixedBurst_Enable;
  ETH_InitStructure.ETH_RxDMABurstLength = ETH_RxDMABurstLength_32Beat;
  ETH_InitStructure.ETH_TxDMABurstLength = ETH_TxDMABurstLength_32Beat;
  ETH_InitStructure.ETH_DMAArbitration = ETH_DMAArbitration_RoundRobin_RxTx_2_1;

  /* Configure Ethernet */
  ETH_Init(&ETH_InitStructure, ETHERNET_PHY_ADDRESS);
}

/**
  * @brief  Configures the different GPIO ports.
  * @param  None
  * @retval None
  */
void ETH_GPIO_Config(void) {
  GPIO_InitTypeDef GPIO_InitStructure;
  
  /* Enable GPIOs clocks */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
                         RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOG,
                         ENABLE);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Configure MCO (PA8) */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;  
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_MCO);

  SYSCFG_ETH_MediaInterfaceConfig(SYSCFG_ETH_MediaInterface_RMII);

/* Ethernet pins configuration ************************************************/
   /*
        ETH_MDIO -------------------------> PA2
        ETH_MDC --------------------------> PC1
        ETH_MII_RX_CLK/ETH_RMII_REF_CLK---> PA1
        ETH_MII_RX_DV/ETH_RMII_CRS_DV ----> PA7
        ETH_MII_RXD0/ETH_RMII_RXD0 -------> PC4
        ETH_MII_RXD1/ETH_RMII_RXD1 -------> PC5
        ETH_MII_TX_EN/ETH_RMII_TX_EN -----> PG11
        ETH_MII_TXD0/ETH_RMII_TXD0 -------> PG13
        ETH_MII_TXD1/ETH_RMII_TXD1 -------> PB13
                                                  */

  /* Configure PA1, PA2 and PA7 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_7;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_ETH);

  /* Configure PB13 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_ETH);

  /* Configure PC1, PC4 and PC5 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource1, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource4, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource5, GPIO_AF_ETH);
  
  /* Configure PG11 and PG13 */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11 | GPIO_Pin_13;
  GPIO_Init(GPIOG, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOG, GPIO_PinSource11, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOG, GPIO_PinSource13, GPIO_AF_ETH);
  
  /* Lock all configuration of ethernet pins */
  GPIO_PinLockConfig(GPIOA, GPIO_Pin_1 );
  GPIO_PinLockConfig(GPIOA, GPIO_Pin_2 );
  GPIO_PinLockConfig(GPIOA, GPIO_Pin_7 );
  GPIO_PinLockConfig(GPIOA, GPIO_Pin_8 );
  
  GPIO_PinLockConfig(GPIOB, GPIO_Pin_13);
  
  GPIO_PinLockConfig(GPIOC, GPIO_Pin_1 );
  GPIO_PinLockConfig(GPIOC, GPIO_Pin_4 );
  GPIO_PinLockConfig(GPIOC, GPIO_Pin_5 );
  
  GPIO_PinLockConfig(GPIOG, GPIO_Pin_11);
  GPIO_PinLockConfig(GPIOG, GPIO_Pin_13);
}

/* This function is called periodically */
/* It checks link status for ethernet controller */
void ETH_CheckLinkStatus(struct netif *netif) {
  uint32_t t = GET_PHY_LINK_STATUS();
  
  /* If we have link */
  if (t) {
    /* Set link up, does nothing if was up */
    netif_set_link_up(netif);
  }
  /* If we don't have link */
  else {
    /* Set link down, does nothing if was down */
    netif_set_link_down(netif);
  }
}

char ETH_CheckPacketReceived(struct netif *netif) {
  if (ETH_CheckFrameReceived()) {
    /* Read a received packet from the Ethernet buffers and send it to the lwIP for handling */
    ethernetif_input(netif);
    return 1;
  }
  return 0;
}

/**
  * @brief  Link callback function, this function is called on change of link status.
  * @param  The network interface
  * @retval None
  */
void ETH_link_callback(struct netif *netif) {
  __IO uint32_t timeout = 0;
 uint32_t tmpreg;
  struct ip_addr ipaddr;
  struct ip_addr netmask;
  struct ip_addr gw;

  if(netif_is_link_up(netif)) {
    /* Restart the autonegotiation */
    if(ETH_InitStructure.ETH_AutoNegotiation != ETH_AutoNegotiation_Disable) {
      /* Reset Timeout counter */
      timeout = 0;

      /* Enable Auto-Negotiation */
      ETH_WritePHYRegister(ETHERNET_PHY_ADDRESS, PHY_BCR, PHY_AutoNegotiation);

      /* Wait until the auto-negotiation will be completed */
      do {
        timeout++;
      } while (!(ETH_ReadPHYRegister(ETHERNET_PHY_ADDRESS, PHY_BSR) & PHY_AutoNego_Complete) && (timeout < (uint32_t)PHY_READ_TO));

      /* Reset Timeout counter */
      timeout = 0;

      /* Get speed and duplex mode from PHY */
      /* This is different for every PHY */
      ETH_EXTERN_GetSpeedAndDuplex(ETHERNET_PHY_ADDRESS, &ETH_InitStructure);

      /*------------------------ ETHERNET MACCR Re-Configuration --------------------*/
      /* Get the ETHERNET MACCR value */  
      tmpreg = ETH->MACCR;

      /* Set the FES bit according to ETH_Speed value */ 
      /* Set the DM bit according to ETH_Mode value */ 
      tmpreg |= (uint32_t)(ETH_InitStructure.ETH_Speed | ETH_InitStructure.ETH_Mode);

      /* Write to ETHERNET MACCR */
      ETH->MACCR = (uint32_t)tmpreg;

      _eth_delay_(ETH_REG_WRITE_DELAY);
      tmpreg = ETH->MACCR;
      ETH->MACCR = tmpreg;
    }

    /* Restart MAC interface */
    ETH_Start();

    if (DHCP_state != DHCP_OFF) {
      ipaddr.addr = 0;
      netmask.addr = 0;
      gw.addr = 0;
      DHCP_state = DHCP_START;
    } else {
      IP4_ADDR(&ipaddr, IP_ADDR0, IP_ADDR1, IP_ADDR2, IP_ADDR3);
      IP4_ADDR(&netmask, NETMASK_ADDR0, NETMASK_ADDR1 , NETMASK_ADDR2, NETMASK_ADDR3);
      IP4_ADDR(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);
    }

    /* Set address */
    netif_set_addr(netif, &ipaddr , &netmask, &gw);

    /* When the netif is fully configured this function must be called.*/
    netif_set_up(netif);
  } else {
    ETH_Stop();
    if (DHCP_state != DHCP_OFF) {
      DHCP_state = DHCP_LINK_DOWN;
      dhcp_stop(netif);
    }

    /*  When the netif link is down this function must be called.*/
    netif_set_down(netif);
  }
}

void ETH_EXTERN_GetSpeedAndDuplex(uint32_t PHYAddress, ETH_InitTypeDef* ETH_InitStruct) {
  uint32_t RegValue;
  
/* DP83848 */
#if ETHERNET_PHY == 0
  /* Read the result of the auto-negotiation */
  RegValue = ETH_ReadPHYRegister(ETHERNET_PHY_ADDRESS, PHY_SR);

  /* Configure the MAC with the Duplex Mode fixed by the auto-negotiation process */
  if ((RegValue & PHY_DUPLEX_STATUS) != (uint16_t)RESET) {
    /* Set Ethernet duplex mode to Full-duplex following the auto-negotiation */
    ETH_InitStruct->ETH_Mode = ETH_Mode_FullDuplex;  
  } else {
    /* Set Ethernet duplex mode to Half-duplex following the auto-negotiation */
    ETH_InitStruct->ETH_Mode = ETH_Mode_HalfDuplex;
  }
  /* Configure the MAC with the speed fixed by the auto-negotiation process */
  if ((RegValue & PHY_SPEED_STATUS) != (uint16_t)RESET) {
    /* Set Ethernet speed to 10M following the auto-negotiation */
    ETH_InitStruct->ETH_Speed = ETH_Speed_10M; 
  } else {
    /* Set Ethernet speed to 100M following the auto-negotiation */
    ETH_InitStruct->ETH_Speed = ETH_Speed_100M;
  }

/* DP83848 */
#elif ETHERNET_PHY == 1
/* LAN8720A */
  /* Read status register, register number 31 = 0x1F */
  RegValue = ETH_ReadPHYRegister(ETHERNET_PHY_ADDRESS, 0x1F);
  /* Mask out bits which are not for speed and link indication, bits 4:2 are used */
  RegValue = (RegValue >> 2) & 0x07;

  /* Switch statement */
  switch (RegValue) {
    case 1: /* Base 10, half-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_10M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_HalfDuplex;
      break;
    case 2: /* Base 100, half-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_100M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_HalfDuplex;
      break;
    case 5: /* Base 10, full-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_10M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_FullDuplex;
      break;
    case 6: /* Base 100, full-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_100M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_FullDuplex;
      break;
    default:
      break;
  }
/* LAN8720A */
#elif ETHERNET_PHY == 2
/* KSZ8081RNA */
  /* Read status register, register number 0x1F */
  RegValue = ETH_ReadPHYRegister(ETHERNET_PHY_ADDRESS, 0x1E);
  /* Mask out bits which are not for speed and link indication, bits 2:0 are used */
  RegValue = (RegValue) & 0x07;

  /* Switch */
  switch (RegValue) {
    case 1: /* Base 10, half-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_10M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_HalfDuplex;
      break;
    case 2: /* Base 100, half-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_100M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_HalfDuplex;
      break;
    case 5: /* Base 10, full-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_10M;
      ETH_InitStructure.ETH_Mode = ETH_Mode_FullDuplex;
      break;
    case 6: /* Base 100, full-duplex */
      ETH_InitStruct->ETH_Speed = ETH_Speed_100M;
      ETH_InitStruct->ETH_Mode = ETH_Mode_FullDuplex;
      break;
    default:
      break;
  }
/* KSZ8081RNA */
#else
  #error "Invalid PHY selected. Open tm_stm32f4_ethernet.h and read manual how to set your PHY from available PHYs!!"
#endif
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
