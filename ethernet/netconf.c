/**
  ******************************************************************************
  * @file    netconf.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    31-July-2013
  * @brief   Network connection configuration
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <lwip/init.h>
#include <lwip/sys.h>
#include <lwip/dhcp.h>
#include <lwip/timers.h>
#include <lwip/netif.h>
#include <netif/etharp.h>
#include <ethernet/netconf.h>
#include <ethernet/ethernetif_stm32f4x7.h>
#include <ethernet/stm32f4x7_eth_bsp.h>
#include <stm32f4x7/stm32f4x7_eth.h>
#include <stdio.h>

#include "hw_uid.h"
#include "debug.h"

/* Private typedef -----------------------------------------------------------*/
#define MAX_DHCP_TRIES        400

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
struct netif gnetif;
struct ip_addr ipaddr;
struct ip_addr netmask;
struct ip_addr gateway;

#define LINK_TIMER_MSECS 1000
#define PCK_RECEIVED_TIMER_MSECS 100
#define MAX_PCK_BURST 10

uint32_t DHCPHandlerTimer = 0;
volatile uint8_t DHCP_state;

/* Private functions ---------------------------------------------------------*/
void link_timer(void *arg);
void HwInitialize(void);
void LwIP_Pkt_Handle(void *arg);
void LwIP_DHCP_Process_Handle(void);

/**
* @brief  Initializes the lwIP stack
* @param  None
* @retval None
*/
void LwIP_Init(void)
{
  ASSERT("Warning: lwip is not release version\n", !LWIP_VERSION_IS_RELEASE);
  
  ipaddr.addr = 0;
  netmask.addr = 0;
  gateway.addr = 0;
  DHCP_state = DHCP_START;
  
  HwInitialize();
  
  // Initialize LWIP
  lwip_init();
  
  /* - netif_add(struct netif *netif, struct ip_addr *ipaddr,
  struct ip_addr *netmask, struct ip_addr *gateway,
  void *state, err_t (* init)(struct netif *netif),
  err_t (* input)(struct pbuf *p, struct netif *netif))

  Adds your network interface to the netif_list. Allocate a struct
  netif and pass a pointer to this structure as the first argument.
  Give pointers to cleared ip_addr structures when using DHCP,
  or fill them with sane numbers otherwise. The state pointer may be NULL.

  The init function pointer must point to a initialization function for
  your ethernet netif interface. The following code illustrates it's use.*/
  
  if (netif_add(&gnetif,
      &ipaddr, &netmask, &gateway, NULL, &ethernetif_init, &ethernet_input) == NULL) {
      LWIP_ASSERT("con_net_init: netif_add (condrv_init) failed\n", 0);
  }
  netif_set_up(&gnetif);
  /*  Registers the default network interface.*/
  netif_set_default(&gnetif);
  netif_set_link_down(&gnetif);

  /* Set the link callback function, this function is called on change of link status*/
  netif_set_link_callback(&gnetif, ETH_link_callback);
  
  /* Implicit call to ETH_link_callback if necessary due to netif_set_link_callback */
  ETH_CheckLinkStatus(&gnetif);
  
  /* Call ETH_CheckLinkStatus periodically */
  sys_timeout(LINK_TIMER_MSECS, link_timer, &gnetif);
  
  sys_timeout(PCK_RECEIVED_TIMER_MSECS, LwIP_Pkt_Handle, &gnetif);
}

void link_timer(void *arg) {
  ETH_CheckLinkStatus((struct netif *)arg);
  sys_timeout(LINK_TIMER_MSECS, link_timer, arg);
}

void HwInitialize(void) {
  // not initialized in lwip_init because NO_SYS
  sys_init();
  
  /* configure ethernet */
  ETH_BSP_Config();
  
  gnetif.hwaddr_len = 6;
  
  uint32_t uid = hw_uid0_31();
  
  gnetif.hwaddr[0] = 0x12;
  gnetif.hwaddr[1] = 0x00;
  
//   gnetif.hwaddr[2] = 0x00;
  gnetif.hwaddr[2] = (uid >> 24) & 0xFF;
  
  gnetif.hwaddr[3] = (uid >> 16) & 0xFF;
  gnetif.hwaddr[4] = (uid >>  8) & 0xFF;
  gnetif.hwaddr[5] = (uid >>  0) & 0xFF;
}

/**
* @brief  Called when a frame is received
* @param  None
* @retval None
*/
void LwIP_Pkt_Handle(void *arg)
{
  uint8_t pktCount = 0;
  while (ETH_CheckPacketReceived(arg)) {
    pktCount++;
    if (pktCount > MAX_PCK_BURST) {
      break;
    }
  }
  sys_timeout(PCK_RECEIVED_TIMER_MSECS, LwIP_Pkt_Handle, arg);
}

void LwIP_Set_IP(struct ip_addr ip, struct ip_addr nm, struct ip_addr gw) {
  netif_set_down(&gnetif);
  ipaddr = ip;
  netmask = nm;
  gateway = gw;
  netif_set_addr(&gnetif, &ipaddr, &netmask, &gateway);
  if (ipaddr.addr == 0 || netmask.addr == 0 || gateway.addr == 0) {
//     dhcp_start(&gnetif);
    DHCP_state = DHCP_START;
  } else {
    DHCP_state = DHCP_OFF;
    dhcp_stop(&gnetif);
    netif_set_up(&gnetif);
  }
}

/**
* @brief  LwIP periodic tasks
* @param  localtime the current LocalTime value
* @retval None
*/
void LwIP_Periodic_Handle(void) {
  sys_check_timeouts();
  /* DHCP Handler periodic process every 500ms */
  if (sys_now() - DHCPHandlerTimer >= 500)
  {
    DHCPHandlerTimer = sys_now();
    if (DHCP_state != DHCP_OFF) {
      if ((DHCP_state != DHCP_ADDRESS_ASSIGNED) &&
          (DHCP_state != DHCP_TIMEOUT) &&
            (DHCP_state != DHCP_LINK_DOWN)) {
        /* process DHCP state machine */
        LwIP_DHCP_Process_Handle();
      }
    }
  }
}

/**
* @brief  LwIP_DHCP_Process_Handle
* @param  None
* @retval None
*/
void LwIP_DHCP_Process_Handle()
{
  struct ip_addr ipaddr;
  struct ip_addr netmask;
  struct ip_addr gw;
  
  switch (DHCP_state)
  {
  case DHCP_START:
    {
      DHCP_state = DHCP_WAIT_ADDRESS;
      dhcp_start(&gnetif);
      gnetif.dhcp->tries = 0;
    }
    break;

  case DHCP_WAIT_ADDRESS:
    {
      if (gnetif.ip_addr.addr!=0) 
      {
        DHCP_state = DHCP_ADDRESS_ASSIGNED;

        /* Stop DHCP */
//         dhcp_stop(&gnetif);
      }
      else
      {
        /* DHCP timeout */
        if (gnetif.dhcp->tries > MAX_DHCP_TRIES)
        {
          DHCP_state = DHCP_TIMEOUT;

          /* Stop DHCP */
          dhcp_stop(&gnetif);

          /* Static address used */
          IP4_ADDR(&ipaddr, IP_ADDR0 ,IP_ADDR1 , IP_ADDR2 , IP_ADDR3 );
          IP4_ADDR(&netmask, NETMASK_ADDR0, NETMASK_ADDR1, NETMASK_ADDR2, NETMASK_ADDR3);
          IP4_ADDR(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);
          netif_set_addr(&gnetif, &ipaddr , &netmask, &gw);
        }
      }
    }
    break;
  default: break;
  }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
