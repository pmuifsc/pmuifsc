
#include <lwip/pbuf.h>
#include <netif/etharp.h>
#include <ethernet/stm32f4x7_eth_bsp.h>
#include "ethernet/enc28j60/enc28j60.h"

#include "debug.h"

static enc_device_t condrv_hw;

char ETH_CheckPacketReceived(struct netif *netif) {
	err_t result;
	struct pbuf *buf = NULL;

	uint8_t epktcnt;
	enc_device_t *encdevice = &condrv_hw;
	epktcnt = enc_RCR(encdevice, ENC_EPKTCNT);

	if (epktcnt) {
    int read_out = enc_read_received_pbuf(encdevice, &buf);
    switch (read_out) {
      case 0:
        LWIP_DEBUGF(NETIF_DEBUG, ("incoming: %d packages, first read into %x\n", epktcnt, (unsigned int)(buf)));
        result = netif->input(buf, netif);
        LWIP_DEBUGF(NETIF_DEBUG, ("received with result %d\n", result));
        return 1;
      case 2:
        // Memory allocation error
        LWIP_DEBUGF(NETIF_DEBUG, ("didn't receive.\n"));
        return 0;
      case -1:
        // enc28j60 error, restart it
        enc_ethernet_setup(encdevice, 4*1024, netif->hwaddr);
      default:
        return 0;
    }
	}
	return 0;
}

static err_t condrv_linkoutput(struct netif *netif, struct pbuf *p)
{
	enc_device_t *encdevice = &condrv_hw;
	enc_transmit_pbuf(encdevice, p);
	LWIP_DEBUGF(NETIF_DEBUG, ("sent %d bytes.\n", p->tot_len));
	/* FIXME: evaluate result state */
	return ERR_OK;
}

err_t ethernetif_init(struct netif *netif) {
	int result;
	enc_device_t *encdevice = &condrv_hw;

	LWIP_DEBUGF(NETIF_DEBUG, ("Starting condrv_init.\n"));

	result = enc_setup_basic(encdevice);
	if (result != 0)
	{
		LWIP_DEBUGF(NETIF_DEBUG, ("Error %d in enc_setup, interface setup aborted.\n", result));
		return ERR_IF;
	}
	result = enc_bist_manual(encdevice);
	if (result != 0)
	{
		LWIP_DEBUGF(NETIF_DEBUG, ("Error %d in enc_bist_manual, interface setup aborted.\n", result));
		return ERR_IF;
	}
	enc_ethernet_setup(encdevice, 4*1024, netif->hwaddr);

	netif->output = etharp_output;
	netif->linkoutput = condrv_linkoutput;

	netif->mtu = 1500; /** FIXME check with documentation when jumboframes can be ok */

	netif->flags |= NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP;

	LWIP_DEBUGF(NETIF_DEBUG, ("Driver initialized.\n"));

	return ERR_OK;
}

void ETH_BSP_Config(void) {
  
}

/* This function is called periodically */
/* It checks link status for ethernet controller */
void ETH_CheckLinkStatus(struct netif *netif) {
  uint32_t t = enc_link_status(netif->state);
  
  /* If we have link */
  if (t) {
    /* Set link up, does nothing if was up */
    netif_set_link_up(netif);
  }
  /* If we don't have link */
  else {
    /* Set link down, does nothing if was down */
    netif_set_link_down(netif);
  }
}

void ETH_link_callback(struct netif *netif) {
  
}
