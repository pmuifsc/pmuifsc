/* ENC28J60 hardware implementation for EMLIB devices.
 *
 * The enchw_device_t multi-device support is unused so far as it was added to
 * enc28j60.c later */

#include "ethernet/enc28j60/enchw.h"
#include "rtc.h"

#include "hw_spi2.h"
#include "hw_gpio.h"

void enchw_setup(enchw_device_t __attribute__((unused)) *dev) {
  hw_spi2_Init();
  hw_gpio_Init(GPIOD, GPIO_Pin_8, GPIO_Mode_OUT); // Reset pin ENC28J60 Module
  hw_gpio_reset(GPIOD, GPIO_Pin_8); // perform Reset
  delay_ms(200);
  hw_gpio_set(GPIOD, GPIO_Pin_8);
  delay_ms(500);
}

void enchw_select(enchw_device_t __attribute__((unused)) *dev) {
  hw_spi2_select();
}

void enchw_unselect(enchw_device_t __attribute__((unused)) *dev) {
  hw_spi2_unselect();
}

uint8_t enchw_exchangebyte(enchw_device_t __attribute__((unused)) *dev, uint8_t byte) {
	return hw_spi2_exchange(byte);
}
