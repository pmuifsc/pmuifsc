
// Implementation sys_arch functions
#include "ethernet/arch/sys_arch.h"
#include "rtc.h"

#include <stdint.h>

void sys_init(void) {
}

uint32_t sys_now(void) {
  return rtc_get_ms();
}
