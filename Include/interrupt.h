#ifndef INTERRUPT_H
#define INTERRUPT_H

void Reset_Handler(void) __attribute__ ((isr));
void NMI_Handler(void) __attribute__ ((isr));
void HardFault_Handler(void) __attribute__ ((isr));
void MemManage_Handler(void) __attribute__ ((isr));
void BusFault_Handler(void) __attribute__ ((isr));
void UsageFault_Handler(void) __attribute__ ((isr));
void SVC_Handler(void) __attribute__ ((isr));
void DebugMon_Handler(void) __attribute__ ((isr));
void PendSV_Handler(void) __attribute__ ((isr));
void SysTick_Handler(void) __attribute__ ((isr));
  
  /* External Interrupts */
void WWDG_IRQHandler (void) __attribute__ ((isr));            /*  Window WatchDog               */
void PVD_IRQHandler (void) __attribute__ ((isr));             /*  PVD through EXTI Line detection  */
void TAMP_STAMP_IRQHandler (void) __attribute__ ((isr));      /*  Tamper and TimeStamps through the EXTI line  */
void RTC_WKUP_IRQHandler (void) __attribute__ ((isr));        /*  RTC Wakeup through the EXTI line  */
void FLASH_IRQHandler (void) __attribute__ ((isr));           /*  FLASH                         */
void RCC_IRQHandler (void) __attribute__ ((isr));             /*  RCC                           */
void EXTI0_IRQHandler (void) __attribute__ ((isr));           /*  EXTI Line0                    */
void EXTI1_IRQHandler (void) __attribute__ ((isr));           /*  EXTI Line1                    */
void EXTI2_IRQHandler (void) __attribute__ ((isr));           /*  EXTI Line2                    */
void EXTI3_IRQHandler (void) __attribute__ ((isr));           /*  EXTI Line3                    */
void EXTI4_IRQHandler (void) __attribute__ ((isr));           /*  EXTI Line4                    */
void DMA1_Stream0_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 0                 */
void DMA1_Stream1_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 1                 */
void DMA1_Stream2_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 2                 */
void DMA1_Stream3_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 3                 */
void DMA1_Stream4_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 4                 */
void DMA1_Stream5_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 5                 */
void DMA1_Stream6_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream 6                 */
void ADC_IRQHandler (void) __attribute__ ((isr));             /*  ADC1, ADC2 and ADC3s          */
void CAN1_TX_IRQHandler (void) __attribute__ ((isr));         /*  CAN1 TX                       */
void CAN1_RX0_IRQHandler (void) __attribute__ ((isr));        /*  CAN1 RX0                      */
void CAN1_RX1_IRQHandler (void) __attribute__ ((isr));        /*  CAN1 RX1                      */
void CAN1_SCE_IRQHandler (void) __attribute__ ((isr));        /*  CAN1 SCE                      */
void EXTI9_5_IRQHandler (void) __attribute__ ((isr));         /*  External Line[9:5]s           */
void TIM1_BRK_TIM9_IRQHandler (void) __attribute__ ((isr));   /*  TIM1 Break and TIM9           */
void TIM1_UP_TIM10_IRQHandler (void) __attribute__ ((isr));   /*  TIM1 Update and TIM10         */
void TIM1_TRG_COM_TIM11_IRQHandler (void) __attribute__ ((isr)); /*  TIM1 Trigger and Commutation and TIM11  */
void TIM1_CC_IRQHandler (void) __attribute__ ((isr));         /*  TIM1 Capture Compare          */
void TIM2_IRQHandler (void) __attribute__ ((isr));            /*  TIM2                          */
void TIM3_IRQHandler (void) __attribute__ ((isr));            /*  TIM3                          */
void TIM4_IRQHandler (void) __attribute__ ((isr));            /*  TIM4                          */
void I2C1_EV_IRQHandler (void) __attribute__ ((isr));         /*  I2C1 Event                    */
void I2C1_ER_IRQHandler (void) __attribute__ ((isr));         /*  I2C1 Error                    */
void I2C2_EV_IRQHandler (void) __attribute__ ((isr));         /*  I2C2 Event                    */
void I2C2_ER_IRQHandler (void) __attribute__ ((isr));         /*  I2C2 Error                    */
void SPI1_IRQHandler (void) __attribute__ ((isr));            /*  SPI1                          */
void SPI2_IRQHandler (void) __attribute__ ((isr));            /*  SPI2                          */
void USART1_IRQHandler (void) __attribute__ ((isr));          /*  USART1                        */
void USART2_IRQHandler (void) __attribute__ ((isr));          /*  USART2                        */
void USART3_IRQHandler (void) __attribute__ ((isr));          /*  USART3                        */
void EXTI15_10_IRQHandler (void) __attribute__ ((isr));       /*  External Line[15:10]s         */
void RTC_Alarm_IRQHandler (void) __attribute__ ((isr));       /*  RTC Alarm (A and B) through EXTI Line  */
void OTG_FS_WKUP_IRQHandler (void) __attribute__ ((isr));     /*  USB OTG FS Wakeup through EXTI line  */
void TIM8_BRK_TIM12_IRQHandler (void) __attribute__ ((isr));  /*  TIM8 Break and TIM12          */
void TIM8_UP_TIM13_IRQHandler (void) __attribute__ ((isr));   /*  TIM8 Update and TIM13         */
void TIM8_TRG_COM_TIM14_IRQHandler (void) __attribute__ ((isr)); /*  TIM8 Trigger and Commutation and TIM14  */
void TIM8_CC_IRQHandler (void) __attribute__ ((isr));         /*  TIM8 Capture Compare          */
void DMA1_Stream7_IRQHandler (void) __attribute__ ((isr));    /*  DMA1 Stream7                  */
void FSMC_IRQHandler (void) __attribute__ ((isr));            /*  FSMC                          */
void SDIO_IRQHandler (void) __attribute__ ((isr));            /*  SDIO                          */
void TIM5_IRQHandler (void) __attribute__ ((isr));            /*  TIM5                          */
void SPI3_IRQHandler (void) __attribute__ ((isr));            /*  SPI3                          */
void UART4_IRQHandler (void) __attribute__ ((isr));           /*  UART4                         */
void UART5_IRQHandler (void) __attribute__ ((isr));           /*  UART5                         */
void TIM6_DAC_IRQHandler (void) __attribute__ ((isr));        /*  TIM6 and DAC1&2 underrun errors  */
void TIM7_IRQHandler (void) __attribute__ ((isr));            /*  TIM7                          */
void DMA2_Stream0_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 0                 */
void DMA2_Stream1_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 1                 */
void DMA2_Stream2_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 2                 */
void DMA2_Stream3_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 3                 */
void DMA2_Stream4_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 4                 */
void ETH_IRQHandler (void) __attribute__ ((isr));             /*  Ethernet                      */
void ETH_WKUP_IRQHandler (void) __attribute__ ((isr));        /*  Ethernet Wakeup through EXTI line  */
void CAN2_TX_IRQHandler (void) __attribute__ ((isr));         /*  CAN2 TX                       */
void CAN2_RX0_IRQHandler (void) __attribute__ ((isr));        /*  CAN2 RX0                      */
void CAN2_RX1_IRQHandler (void) __attribute__ ((isr));        /*  CAN2 RX1                      */
void CAN2_SCE_IRQHandler (void) __attribute__ ((isr));        /*  CAN2 SCE                      */
void OTG_FS_IRQHandler (void) __attribute__ ((isr));          /*  USB OTG FS                    */
void DMA2_Stream5_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 5                 */
void DMA2_Stream6_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 6                 */
void DMA2_Stream7_IRQHandler (void) __attribute__ ((isr));    /*  DMA2 Stream 7                 */
void USART6_IRQHandler (void) __attribute__ ((isr));          /*  USART6                        */
void I2C3_EV_IRQHandler (void) __attribute__ ((isr));         /*  I2C3 event                    */
void I2C3_ER_IRQHandler (void) __attribute__ ((isr));         /*  I2C3 error                    */
void OTG_HS_EP1_OUT_IRQHandler (void) __attribute__ ((isr));  /*  USB OTG HS End Point 1 Out    */
void OTG_HS_EP1_IN_IRQHandler (void) __attribute__ ((isr));   /*  USB OTG HS End Point 1 In     */
void OTG_HS_WKUP_IRQHandler (void) __attribute__ ((isr));     /*  USB OTG HS Wakeup through EXTI  */
void OTG_HS_IRQHandler (void) __attribute__ ((isr));          /*  USB OTG HS                    */
void DCMI_IRQHandler (void) __attribute__ ((isr));            /*  DCMI                          */
void CRYP_IRQHandler (void) __attribute__ ((isr));            /*  CRYP crypto                   */
void HASH_RNG_IRQHandler (void) __attribute__ ((isr));        /*  Hash and Rng                  */
void FPU_IRQHandler (void) __attribute__ ((isr));             /*  FPU                           */
void UART7_IRQHandler (void) __attribute__ ((isr));           /* UART7                        */
void UART8_IRQHandler (void) __attribute__ ((isr));           /* UART8                        */
void SPI4_IRQHandler (void) __attribute__ ((isr));            /* SPI4                         */
void SPI5_IRQHandler (void) __attribute__ ((isr));            /* SPI5                         */
void SPI6_IRQHandler (void) __attribute__ ((isr));            /* SPI6                         */
void SAI1_IRQHandler (void) __attribute__ ((isr));            /* SAI1                         */
void LTDC_IRQHandler (void) __attribute__ ((isr));            /* LTDC                         */
void LTDC_ER_IRQHandler (void) __attribute__ ((isr));         /* LTDC error                   */
void DMA2D_IRQHandler (void) __attribute__ ((isr));           /* DMA2D                        */
  /* External Interrupts */
  
 
 


#endif
