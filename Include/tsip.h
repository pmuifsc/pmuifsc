#ifndef TSIP_H
#define TSIP_H
#include "tsip.h"

#include <stdint.h>


void tsipInit();
uint8_t tsipPool();

#endif
