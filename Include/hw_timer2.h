#ifndef HW_TIMER2_H
#define HW_TIMER2_H

#include <stdint.h>

#define TIMER2_BASE 12000000

void hw_timer2_Init(void);
void hw_timer2_start(void);
void hw_timer2_restart(void);
uint32_t hw_timer2_reset(void);
void hw_timer2_stop(void);
uint32_t hw_timer2_read(void);
uint32_t hw_timer2_maximum(void);

void hw_timer2_external(void);
void hw_timer2_interrupt(void);
uint8_t hw_timer2_opStatus(void);

void hw_timer2_attachISR(void (*interrupt)(void));

#endif
