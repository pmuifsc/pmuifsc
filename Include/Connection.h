#ifndef CONNECTION_H
#define CONNECTION_H

#include <stdint.h>

void connectionInit(void);

void connectionSendDataAll(char *dataToSend, int len);

void connectionPool(void);

#endif
