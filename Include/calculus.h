#ifndef CALCULUS_H
#define CALCULUS_H

#include <stdint.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>

#include "core/arm_math.h"
#include "core/arm_common_tables.h"
//#include "core/arm_const_structs.h"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795L
#endif

// Put next two param in configure 'class'
#define NSAMPLES 500
#define F_FREQ (float)60.0 //fundamental frequency 60Hz

#define T_SAMPLE  ((uint32_t)(12000000/(F_FREQ*NSAMPLES)) / 12000000.0)  //periodo de amostragem

#define TRANSFORM_RELATION_A  468.640167364
#define TRANSFORM_RELATION_B  465.077720207
#define TRANSFORM_RELATION_C  466.693944354
#define ADC_VOLTAGE_REF     3.3000
#define ADC_VOLTAGE_RANGE   4096
#define RMS_RELATION        (1.4142135623730950488016887242097)

typedef struct Complex {
    float real;
    float imag;
} Complex;

typedef struct PowerLineInfo {
    Complex phasor[3];
    float freq;
    uint32_t timestamp;
    uint32_t fracsec;
} PowerLineInfo;

double angle(Complex num);
float rad2deg(double rad);
float corrigeQuadrante(Complex phasor, Complex phasor_n1);
Complex c_sum(Complex a, Complex b);
Complex c_mult(Complex a, Complex b);
Complex c_diff(Complex a, Complex b);

int dotDivisionScalar(float * result, float scalar_b, float * frequencia, int num_ciclos);

uint8_t phaseDetection(int16_t *samples);
int powerLineEvaluate(int16_t *samples1, int16_t *samples2, PowerLineInfo *res);
int calcPhasorFreq(int16_t *samples1, int16_t *samples2, PowerLineInfo *res, uint8_t phaseToCalc);
void lowPassFilter(uint16_t *sampi1, uint16_t *sampi2, uint16_t *sampo1, uint16_t *sampo2);
void setIterations(uint8_t);
uint8_t getIterations(void);
void resetK();

#endif
