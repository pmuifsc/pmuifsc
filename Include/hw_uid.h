#ifndef HW_UID_H
#define HW_UID_H

#include <stdint.h>

void hw_uid_Init(void);

uint32_t hw_uid0_31(void);
uint32_t hw_uid32_63(void);
uint32_t hw_uid64_95(void);

uint64_t hw_uid0_63(void);

#endif
