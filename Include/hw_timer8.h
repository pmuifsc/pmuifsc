#ifndef HW_TIMER8_H
#define HW_TIMER8_H

#include <stdint.h>

void hw_timer8_Init(float powerFrequency, uint16_t nsamples);
void hw_timer8_start(void);
void hw_timer8_restart(void);
uint32_t hw_timer8_reset(void);
void hw_timer8_stop(void);
uint32_t hw_timer8_read(void);
void hw_timer8_set_err(int16_t error);

#endif
