#ifndef HW_GPIO_H
#define HW_GPIO_H

#include <stdint.h>

#include "stm32f4xx_conf.h"
#include "stm32f4xx/stm32f4xx_gpio.h"

void hw_gpio_Init(GPIO_TypeDef* GPIOx, uint32_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode);
void hw_gpio_set(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void hw_gpio_reset(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

#endif

