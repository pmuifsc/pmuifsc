#ifndef HW_UART2_H
#define HW_UART2_H

#include <stdlib.h>

void uart2Print(const char *str);

void hw_uart2_Init();
void hw_uart2_destroy();
char hw_uart2_send(unsigned char c);
char hw_uart2_read(char *readed);

#endif
