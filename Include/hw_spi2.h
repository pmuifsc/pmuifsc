#ifndef HW_SPI2_H
#define HW_SPI2_H

#include <stdint.h>

void hw_spi2_Init();
void hw_spi2_destroy();

void hw_spi2_select() __attribute__((inline));
void hw_spi2_unselect() __attribute__((inline));

char hw_spi2_send(char byte) __attribute__((inline));
char hw_spi2_read(void) __attribute__((inline));

char hw_spi2_exchange(char byte) __attribute__((inline));

#endif
