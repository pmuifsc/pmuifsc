#ifndef HW_SPI1_H
#define HW_SPI1_H

#include <stdint.h>

void hw_spi1_Init();
void hw_spi1_lowSpeed();
void hw_spi1_highSpeed();
void hw_spi1_set8bits(void);
void hw_spi1_set16bits(void);
void hw_spi1_destroy();
void hw_spi1_select();
void hw_spi1_unselect();
char hw_spi1_send(uint16_t data);
uint16_t hw_spi1_read(void);
uint16_t hw_spi1_exchange(uint16_t data);

#endif
