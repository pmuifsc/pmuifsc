#ifndef HW_ADC_H
#define HW_ADC_H

#include <stdint.h>

#define STAMP_SOC       0
#define STAMP_FRAC      1

#define COUNTSAMPLES    4000

extern volatile uint16_t adcBuffer[4][COUNTSAMPLES*3];
extern volatile uint8_t  lastUsedBuffer;
extern volatile uint8_t  dirtyBuffer[4];
extern volatile uint32_t bufferTimeStamp[4][2];
extern volatile float diffPhaseInASec;
extern volatile uint8_t samplesSynced;

extern int32_t crystalErr;

void hw_adc_Init(float nominalFrequency);
void hw_adc_reset(void);

#endif
