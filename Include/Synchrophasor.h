#ifndef SYNCHROPHASOR_H
#define SYNCHROPHASOR_H

#include <stdint.h>

// PowerLineInfo and Complex Structs needed
#include "calculus.h"


// struct definitions
// external variables

void synchrophasorInit(void);

// Getters setters of configuration of pmu
// Maybe a struct defining some variables

// Encode data in IEEE C37.118.2 standard

uint8_t *getConfigurationFrame(uint16_t *length);

uint8_t *getDataFrame(uint16_t *length, PowerLineInfo *lineData);

#endif
