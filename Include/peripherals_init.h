#ifndef __PERIPHERALS_INIT_H
#define __PERIPHERALS_INIT_H

/* Include core modules */
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_it.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_dac.h"

/* Includes to use TM functions */
#include "defines.h"
#include "tm_stm32f4_delay.h"
#include "tm_stm32f4_disco.h"
#include "tm_stm32f4_usart.h" 
#include "tm_stm32f4_stdio.h"
#include "tm_stm32f4_rtc.h"
#include <stdio.h>

/* DAC stuff */
#define   COSINE_FREQ       60                                  // Output waveform frequency
#define   COSINE_RES        128                                 // Waveform resolution
#define   DAC_DHR12R1_ADDR  0x40007408                          // DMA writes into this reg on every request, DAC Channel1 12 bit-right aligned data holding register
#define   TIM_CLK_APB1_FREQ   84000000                     // Clock for all timers in APB1 bus, since PB1 prescaler isn't 1, so Timer clock is PCLK1*2
#define   TIM_CLK_APB2_FREQ   168000000                    // Clock for all timers in APB2 bus, since PB1 prescaler isn't 1, so Timer clock is PCLK2*2
#define   DAC1_PERIOD     ((TIM_CLK_APB1_FREQ)/((COSINE_RES)*(COSINE_FREQ))) // Autoreload reg value    

/* ADC stuff */
#define NSAMPLES       512       // Number of power grid samples to get from ADC
#define BUFFERSIZE     256
#define ADC1_RDR 0x4001204C      // ADC1 Regular Data Register (read only)
#define FFREQ 60                //fundamental power grid cosine frequency
#define ADC1_PERIOD ((1/FFREQ)/NSAMPLES)
extern volatile uint16_t adc_dma_buffer0[BUFFERSIZE];   // DMA using dual buffer mode: 2x256=512 samples/cycle
extern volatile uint16_t adc_dma_buffer1[BUFFERSIZE];
//====================================================================================
//   Functions used for measuring temperature variations
//====================================================================================
/* USART stuff */
/* You can use only 1 thing for printf */
/* If you want more, then you can use fprintf */
/* For this you need FILE pointer for each */
/* Create global variables for fprintf function */
/* for USART2 and USART6 */

int USART6_Stream_InputFunction(TM_FILE* f); //defunes stdin handler for usart used with file descriptor
int USART6_Stream_OutputFunction(int ch, TM_FILE* f); //defines stdout usart used with file descriptor

void DAC1_CLK_Config(void);
void DAC1_GPIO_Config(void);
void DAC1_Sine_Config(void);  

void ADC1_Interrupt_Config(void);    
void ADC1_Config(void);
void ADC1_GPIO_Config(void);

void ADC1_CLK_Config(void);
void ADC1_CLK_Interrupt_Config(void);
void ADC1_CLK_OUT_GPIO_Config(void);
void ADC1_DMA_Interrupt_Config(void);

void DAC2_DEBUG_GPIO_Config(void);
void DAC2_DEBUG_Config(void);

void Timestamp_Config(void);
void Timestamp_Interrupt_Config(void);
void Timestamp_OUT_GPIO_Config(void);

void PPS_Config(void);
void PPS_Interrupt_Config(void);
void PPS_OUT_GPIO_Config(void);

void PC0_EXTI_Config(void);

#endif // __PERIPHERALS_INIT_H
