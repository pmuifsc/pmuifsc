#ifndef HW_TIMER5_H
#define HW_TIMER5_H

#include <stdint.h>

void hw_timer5_Init(void);
void hw_timer5_start(void);
void hw_timer5_restart(void);
uint32_t hw_timer5_reset(void);
void hw_timer5_stop(void);
uint32_t hw_timer5_read(void);
void hw_timer5_attachISR(void (*callback)(void));

#endif
