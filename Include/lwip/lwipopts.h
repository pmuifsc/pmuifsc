#ifndef MY__LWIPOPTS_H__
#define MY__LWIPOPTS_H__

/** 
 * NO_SYS==1: Provides VERY minimal functionality. Otherwise,
 * use lwIP facilities.
 */
#define NO_SYS                          1

/* Disable the more complex APIs */
#define LWIP_SOCKET 0
#define LWIP_NETCONN 0

/* Disable subsystems that eat memory and are not needed in this example */
#define IP_REASSEMBLY                   1
#define LWIP_TCP                        0
// #define PBUF_POOL_SIZE                  0
// Enable necessary modules
#define MEMP_NUM_UDP_PCB                8
#define LWIP_DHCP                       1
#define LWIP_DNS                        1

// use mem pools lwip implementation
#define MEMP_MEM_MALLOC                 0
#define MEM_USE_POOLS                   0

#define MEM_LIBC_MALLOC                 0
#define MEMP_LIBC_MALLOC                0

#define LWIP_NETIF_LINK_CALLBACK        1

#define LWIP_DEBUG                      0
#define LWIP_DEBUG_TIMERNAMES           0
#define DHCP_DEBUG                      (LWIP_DBG_OFF)
#define TIMERS_DEBUG                    (LWIP_DBG_OFF)
#define LWIP_DBG_MIN_LEVEL              LWIP_DBG_LEVEL_ALL
#define LWIP_NOASSERT

/* add one timeout for testapp.c to the default timeouts */
#define MEMP_NUM_SYS_TIMEOUT            16
// #define MEMP_NUM_SYS_TIMEOUT            (LWIP_TCP + IP_REASSEMBLY + LWIP_ARP + (2*LWIP_DHCP) + LWIP_AUTOIP + LWIP_IGMP + LWIP_DNS + PPP_SUPPORT + 1)

#endif /* MY__LWIPOPTS_H__ */
