#ifndef DEBUG_H
#define DEBUG_H

#include "hw_uart1.h"
#include "string.h"

#define DEBUG


#ifdef DEBUG

#define EXPANDED__LINE__

#ifndef ASSERT_CRITICAL
#define ASSERT_CRITICAL(str, cond)              \
if (cond) {                                     \
  uart1Print("CRITICAL ERROR:");                \
  uart1Print(__FILE__);                         \
  hw_uart1_send(':');                           \
  {char tmp[20]; sprintf(tmp,"%d",__LINE__); uart1Print(tmp); } \
  hw_uart1_send('\n');                          \
  uart1Print(str);                              \
  hw_uart1_send('\n');                          \
  while(1);                                     \
}
#endif /* ASSERT_CRITICAL */

#ifndef ASSERT_ERROR
#define ASSERT_ERROR(str, cond, finally)         \
if (cond) {                                     \
  uart1Print("ERROR:");                         \
  uart1Print(__FILE__);                         \
  hw_uart1_send(':');                           \
  {char tmp[20]; sprintf(tmp,"%d",__LINE__); uart1Print(tmp); } \
  hw_uart1_send('\n');                          \
  uart1Print(str);                              \
  hw_uart1_send('\n');                          \
  finally;                                      \
}
#endif /* ASSERT_ERROR */

#ifndef ASSERT
#define ASSERT(str, cond) \
if (cond) {                                     \
  uart1Print("ASSERT:");                        \
  uart1Print(__FILE__);                         \
  hw_uart1_send(':');                           \
  {char tmp[20]; sprintf(tmp,"%d",__LINE__); uart1Print(tmp); } \
  hw_uart1_send('\n');                          \
  uart1Print(str);                              \
  hw_uart1_send('\n');                          \
}
#endif /* ASSERT */

#else /* DEBUG */

#define ASSERT_CRITICAL(str, cond)
#define ASSERT_ERROR(str, cond, finally)
#define ASSERT(str, cond)

#endif /* DEBUG */

#endif
