#ifndef HW_EXTI0_H
#define HW_EXTI0_H

#include <stdint.h>

// Set when refresh, must be cleared by user
extern volatile char diffTimeTrigger;

void hw_exti0_Init(void);
void hw_exti0_attachISR(void (*interrupt)(void));
int32_t hw_exti0_time(void);
float hw_exti0_error(void);

#endif
