#ifndef HW_UART1_H
#define HW_UART1_H

void uart1Print(const char *str);

void hw_uart1_Init();
void hw_uart1_destroy();
char hw_uart1_send(unsigned char c);
char hw_uart1_read(char *readed);

#endif
