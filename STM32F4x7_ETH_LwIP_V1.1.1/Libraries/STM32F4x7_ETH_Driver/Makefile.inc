#*****************************************************************************#
# Makefile.inc: Some variable definitions to build CMSIS Peripherals.         #
#-----------------------------------------------------------------------------#
# Jesús Alonso Fernández @doragasu, 2012                                      #
#*****************************************************************************#

# PART: The chip used for the build
# TODO: Make some conditional assignments to CFLAGS depending on PART
PART = STM32F429ZI
# Prefix to the cross compiler
PREFIX = arm-none-eabi-
# Common stuff
CC = gcc
AR = ar
RM = rm
MKDIR = mkdir
LD = ld
OBJCOPY = objcopy
# Some directories
OBJDIR = $(ROOT)/obj
INCDIR = $(ROOT)/inc
LIBDIR = $(ROOT)/lib
LIB = $(LIBDIR)/libeth_stm32f4.a
STINCDIR= $(TOP)/Include
CMSISINCDIR= $(ROOT)/../CMSIS/Include

# Flags for building stuff
CFLAGS =      \
  -mthumb     \
  -mcpu=cortex-m4   \
  -mfpu=fpv4-sp-d16 \
  -mfloat-abi=hard  \
  -Os     \
  -ffunction-sections \
  -fdata-sections   \
  -std=c99    \
  -Wall     \
  -DPART_${PART} -DSTM32F429_439xx \
  -DARM_MATH_CM4    \
  -D__FPU_PRESENT   \
  -DUSE_STDPERIPH_DRIVER \
  -DSTM32F4XX \
  -DHSE_VALUE=8000000 \
  -I$(INCDIR) -I$(CMSISINCDIR) -I$(STINCDIR) -I$(STINCDIR)/stm32f4xx/ -I$(STINCDIR)/ethernet/

