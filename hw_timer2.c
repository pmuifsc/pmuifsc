
#include "hw_timer2.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_tim.h"
#include "stm32f4xx/stm32f4xx_rcc.h"
#include "stm32f4xx/stm32f4xx_gpio.h"
#include "stm32f4xx/misc.h"
#include "hw_uart1.h"

#include <stdarg.h>
#include <stdio.h>

#define CLOCK_INPUT_PORT  GPIOA
#define CLOCK_INPUT_PIN   GPIO_Pin_5
#define CLOCK_INPUT_PIN_S GPIO_PinSource5

// 28 is default value
#define NVIC_PRIORITY 4

static void dummy(void) {
  return;
}

static uint8_t operationError;

static void (*interruptCallBack)(void) = NULL;

void hw_timer2_Init() {
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  // Configure to full scale
  TIM_TimeBaseStructInit(&TIM_TimeBaseInitStruct);

#ifdef STM32F40_41xxx
  TIM_TimeBaseInitStruct.TIM_Prescaler = ((SystemCoreClock/2) / 12000000)-1;
#elif STM32F429_439xx
  TIM_TimeBaseInitStruct.TIM_Prescaler = ((SystemCoreClock) / 12000000)-1;
#endif
  TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period = (2*TIMER2_BASE)-1; // Maximun
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x0000;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);

  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);
  TIM_SetCounter(TIM2, 0);

  operationError = 1;
#if 1
  // define a dummy callback function
  if (interruptCallBack == NULL) {
    interruptCallBack = dummy;
  }

  NVIC_InitTypeDef nviccfg;
  // Configure Interrupt priority in NVIC
  nviccfg.NVIC_IRQChannel = TIM2_IRQn;
  nviccfg.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  nviccfg.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  nviccfg.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nviccfg);

  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
#endif
}

void hw_timer2_start() {
  operationError = 0;
  TIM_Cmd(TIM2, ENABLE);
}

void hw_timer2_restart() {
  TIM_SetCounter(TIM2, 0);
  TIM_Cmd(TIM2, ENABLE);
}

uint32_t hw_timer2_reset() {
  uint32_t tmp = TIM_GetCounter(TIM2);
  TIM_SetCounter(TIM2, 0);
  return tmp;
}

void hw_timer2_stop() {
  TIM_Cmd(TIM2, DISABLE);
}

uint32_t hw_timer2_read() {
  return TIM_GetCounter(TIM2);
}

void hw_timer2_interrupt(void) {
  if (operationError == 0) {
    interruptCallBack();
  }
  uart1Print("ERROR SYNC GPS");
  operationError = 1;
  TIM_SetAutoreload(TIM2, TIMER2_BASE-1);
}

void hw_timer2_external(void) {
  operationError = 0;
  TIM_SetAutoreload(TIM2, (2*TIMER2_BASE)-1);
}

uint8_t hw_timer2_opStatus(void) {
  return operationError;
}

uint32_t hw_timer2_maximum(void) {
  return TIMER2_BASE;
}

void hw_timer2_attachISR(void (*interrupt)(void)) {
  interruptCallBack = interrupt;
}

#if 1
#include "interrupt.h"
void TIM2_IRQHandler(void) {
  hw_timer2_interrupt();
  //interruptCallBack();
  TIM_ClearFlag(TIM2, TIM_FLAG_Update);
}
#endif
