
#include "hw_uart2.h"

#include "stm32f4xx.h"
#include "stm32f4xx/stm32f4xx_usart.h"
#include "stm32f4xx/stm32f4xx_gpio.h"
#include "stm32f4xx/misc.h"


#include <stdarg.h>
#include <stdio.h>

#define RX_PIN    GPIO_Pin_6
#define RX_PIN_S  GPIO_PinSource6
#define RX_PORT   GPIOD
#define TX_PIN    GPIO_Pin_5
#define TX_PIN_S  GPIO_PinSource5
#define TX_PORT   GPIOD

#define USART_BAUDRATE 9600

// 38 is default value
#define NVIC_PRIORITY 8

static char wBuffer[256];
static char rBuffer[256];
volatile static uint8_t wHead, wTail;
volatile static uint8_t rHead, rTail;

#if 0
// Print formated definition not in use yet
void uart2Printf(char *tmpBuffer, const char *str, ...) {
  va_list arg;
  va_start (arg, str);
  vsprintf (tmpBuffer, str, arg);
  va_end (arg);
  return;
}
#endif

void uart2Print(const char *str) {
  for (;*str; str++) {
    hw_uart2_send(*str);
  }
}

void hw_uart2_Init() {
  NVIC_InitTypeDef nviccfg;
  GPIO_InitTypeDef gpiocfg;
  USART_InitTypeDef usartcfg;
  // Enable clock to Peripheral USART2
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
  // Initialize UART values
  USART_StructInit(&usartcfg);
  // Overwrite Default Baudrate. Default == 9600
  usartcfg.USART_BaudRate = USART_BAUDRATE;
  usartcfg.USART_Parity = USART_Parity_Odd;
  USART_Init(USART2, &usartcfg);
  USART_Cmd(USART2, ENABLE);
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

  // GPIO Init
  GPIO_StructInit(&gpiocfg);
  gpiocfg.GPIO_Mode = GPIO_Mode_AF;
  gpiocfg.GPIO_Speed = GPIO_Speed_100MHz;
  // Set GPIO pins to USART2 RX Function
  gpiocfg.GPIO_Pin = RX_PIN;
  GPIO_Init(RX_PORT, &gpiocfg);
  GPIO_PinAFConfig(RX_PORT, RX_PIN_S, GPIO_AF_USART2);

  // Set GPIO pins to USART2 TX Function
  gpiocfg.GPIO_Pin = TX_PIN;
  GPIO_Init(TX_PORT, &gpiocfg);
  GPIO_PinAFConfig(TX_PORT, TX_PIN_S, GPIO_AF_USART2);

  // Ensure the pin configuration dont be overwrited by another peripheral
  // Comment if you want to disable uart and use those pins
  GPIO_PinLockConfig(TX_PORT, TX_PIN);
  GPIO_PinLockConfig(RX_PORT, RX_PIN);

  // Configure Interrupt priority in NVIC
  nviccfg.NVIC_IRQChannel = USART2_IRQn;
  nviccfg.NVIC_IRQChannelPreemptionPriority = NVIC_PRIORITY / 16;
  nviccfg.NVIC_IRQChannelSubPriority = NVIC_PRIORITY % 16;
  nviccfg.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nviccfg);

  // Object initializations
  wHead = 0;
  wTail = 0;
  rHead = 0;
  rTail = 0;
}

void hw_uart2_destroy() {
  NVIC_InitTypeDef nviccfg;
  nviccfg.NVIC_IRQChannel = USART2_IRQn;
  nviccfg.NVIC_IRQChannelCmd = DISABLE;
  NVIC_Init(&nviccfg);
  USART_DeInit(USART2);
  wHead = 0;
  wTail = 0;
  rHead = 0;
  rTail = 0;
}

char hw_uart2_send(unsigned char c) {
  if (wHead + 1 == wTail)
    return -1;
  USART_ITConfig(USART2, USART_IT_TC, DISABLE);
  wBuffer[wHead++] = c;
  USART_ITConfig(USART2, USART_IT_TC, ENABLE);
  return 0;
}

char hw_uart2_read(char *readed) {
  if (rHead == rTail) {
    *readed = 0;
    return -1;
  }
  *readed  = rBuffer[rTail++];
  return 0;
}

#include "interrupt.h"

void USART2_IRQHandler(void) {
  if (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == SET) {
    char ch = USART_ReceiveData(USART2);
    if (rHead + 1 != rTail) {
      rBuffer[rHead++] = ch;
    }
  }
  if (USART_GetFlagStatus(USART2, USART_FLAG_TC) == SET) {
    if (wHead == wTail) {
      USART_ITConfig(USART2, USART_IT_TC, DISABLE);
    } else {
      USART_SendData(USART2, wBuffer[wTail++]);
    }
  }
}
