
#include "Connection.h"

#include <lwip/inet.h>
#include <lwip/udp.h>
#include <lwip/dns.h>
#include <lwip/dhcp.h>
#include <lwip/sys.h>

#include "ethernet/netconf.h"
#include "rtc.h"

#include "hw_uid.h"

#include "debug.h"

const char server1[] = "172.16.111.76";
const uint16_t port1 = 4713;

const char server2[] = "150.162.19.214";
const uint16_t port2 = 5013;


enum pmuSyncState {
  IDLE,
  DNS_IN_PROGRESS,
  DNS_SOLVED,
  SENDING
};

struct Syncrophasor {
  const char *server;
  struct ip_addr srv_IP;
  uint16_t port;
  struct udp_pcb *pcb;
  enum pmuSyncState state;
} pmuSyncLab, pmuSyncSincroplan;

static void recv(void __attribute__((unused)) *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, uint16_t port);
static void setDNS(const char *name, struct ip_addr *ipaddr, void *arg);

void initSyncrophasor(struct Syncrophasor *pmuSync, const char *server, int port);
void connectionSendData(struct Syncrophasor *pmuSync, char *dataToSend, int len);

void connectionInit(void) {
  // Initializes Netconf
  LwIP_Init();

  initSyncrophasor(&pmuSyncLab,        server1, port1);
  initSyncrophasor(&pmuSyncSincroplan, server2, port2);

  udp_recv(pmuSyncLab.pcb, recv, NULL);
  udp_bind(pmuSyncLab.pcb, IP_ADDR_ANY, 4712);
}

void initSyncrophasor(struct Syncrophasor *pmuSync, const char *server, int port) {
  pmuSync->server = server;
  pmuSync->srv_IP.addr = 0x00000000UL;
  pmuSync->port = port;
  pmuSync->pcb = udp_new();
  pmuSync->state = IDLE;
  ASSERT_ERROR("pmuSync->pcb is NULL\n", pmuSync->pcb == NULL, return);
}

void connectionSendDataAll(char *dataToSend, int len) {
  connectionSendData(&pmuSyncLab, dataToSend, len);
  connectionSendData(&pmuSyncSincroplan, dataToSend, len);
}

void connectionSendData(struct Syncrophasor *pmuSync, char *dataToSend, int len) {
  struct pbuf *response = NULL;
  struct pbuf *message = NULL;
  err_t err_result;

  // Check if have connection
  if ((gnetif.flags & NETIF_FLAG_LINK_UP) == 0 ||
      gnetif.dhcp->state != DHCP_BOUND ||
      dataToSend == NULL || len == 0
      ) {
    pmuSync->state = IDLE;
    return;
  }

  switch(pmuSync->state) {
    case IDLE:
      err_result = dns_gethostbyname(pmuSync->server, &pmuSync->srv_IP,
                                           setDNS, pmuSync);
      if (err_result == ERR_OK) {
        pmuSync->state = SENDING;
      } else if (err_result == ERR_INPROGRESS) {
        pmuSync->state = DNS_IN_PROGRESS;
        return;
      } else {
        ASSERT("dns_gethostbyname error", 1);
        pmuSync->srv_IP.addr = 0x00000000UL;
        return;
      }
      break;
    case DNS_IN_PROGRESS:
      return;
    case DNS_SOLVED:
      pmuSync->state = SENDING;
      break;
    case SENDING:
      break;
    default:
      return;
  }

  response = pbuf_alloc(PBUF_TRANSPORT, 0, PBUF_RAM);
  ASSERT_ERROR("response is NULL", response==NULL, return);
  message = pbuf_alloc(PBUF_RAW, len, PBUF_REF);
  ASSERT_ERROR("message is NULL", message==NULL, pbuf_free(response);return);
  /* by casting from const char* to char*, we acknowledge to the compiler
   * that we rely on lwIP never to write to the payload of a PBUF_ROM
   * type buffer. (it can't know that by itself, so we'd get a
   * "assignment discards `const' qualifier from pointer target type"
   * warning). */
  message->payload = dataToSend;
  // concatenate response and message
  pbuf_cat(response, message);
  err_result = udp_sendto(pmuSync->pcb, response, &pmuSync->srv_IP, pmuSync->port);
  ASSERT("udp_sendto", err_result!=ERR_OK);

  pbuf_free(response);
}

void connectionPool(void) {
  // pooling to check received packets and timouts
  LwIP_Periodic_Handle();

#if 1 // DEBUG
  uint32_t timeNow = sys_now();
  char tmp[100];
  static uint32_t tmr = -1;
  if (timeNow >= tmr + 1000 || timeNow < tmr) {
#if 1
    uint32_t ip = gnetif.dhcp->offered_ip_addr.addr;
//     uint32_t ip = gnetif.ip_addr.addr;
    sprintf(tmp, "IP: %lu.%lu.%lu.%lu %16llu %9d\n",
            ip >> 0 & 0xFF,
            ip >> 8 & 0xFF,
            ip >> 16 & 0xFF,
            ip >> 24 & 0xFF,
#else
            sprintf(tmp, "TIME: %lu\tDHCP: %d\n",
#endif // show ip, !dont show
#if 0
            (unsigned int)ip,
#else
            rtc_get64(),
#endif // !timeNow, ip
            gnetif.dhcp->state);
    uart1Print(tmp);
    tmr = timeNow;
  }
#endif // DEBUG
}

// callback
static void
recv(void __attribute__((unused)) *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, uint16_t port)
{
  struct pbuf *response;
  struct pbuf *message;

  const char teststr[] =
  { 0xAA, 0x31,
    0x01, 0xC6,
    0x1E, 0x36,
    0x44, 0x85, 0x27, 0xF0,
    0x56, 0x07, 0x10, 0x98,
    0x00, 0x0F, 0x42, 0x40,
    0x00, 0x01,
    'S','t','a','t','i','o','n',' ','A',' ',' ',' ',' ',' ',' ',' ',
    0x1E, 0x36,
    0x00, 0x04,
    0x00, 0x04,
    0x00, 0x03,
    0x00, 0x01,
    'V','A',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'V','B',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'V','C',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'I','1',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'A','N','A','L','O','G','1',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'A','N','A','L','O','G','2',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'A','N','A','L','O','G','3',' ',' ',' ',' ',' ',' ',' ',' ',' ',
    'B','R','E','A','K','E','R',' ','1',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','2',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','3',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','4',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','5',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','6',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','7',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','8',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','9',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','A',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','B',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','C',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','D',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','E',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','F',' ','S','T','A','T','U','S',
    'B','R','E','A','K','E','R',' ','G',' ','S','T','A','T','U','S',
    0x00, 0x0D, 0xF8, 0x47,
    0x00, 0x0D, 0xF8, 0x47,
    0x00, 0x0D, 0xF8, 0x47,
    0x01, 0x00, 0xB2, 0xD0,
    0x00, 0x00, 0x00, 0x01,
    0x01, 0x00, 0x00, 0x01,
    0x02, 0x00, 0x00, 0x01,
    0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x00,
    0x00, 0x16,

    0x00, 0x1E,
    0xD5, 0xD1
  };

  response = pbuf_alloc(PBUF_TRANSPORT, 0, PBUF_RAM);
  ASSERT_ERROR("response is NULL", response == NULL, pbuf_free(p);return);
  message = pbuf_alloc(PBUF_RAW, sizeof(teststr), PBUF_ROM);
  ASSERT_ERROR("message is NULL", message==NULL, pbuf_free(response);pbuf_free(p);return);
  /* by casting from const char* to char*, we acknowledge to the compiler
   * that we rely on lwIP never to write to the payload of a PBUF_ROM
   * type buffer. (it can't know that by itself, so we'd get a
   * "assignment discards `const' qualifier from pointer target type"
   * warning). */
  // 	message->payload = (char*)message_text;
  message->payload = (char *) teststr;
  pbuf_cat(response, message);

  udp_sendto(pcb, response, addr, port);

  pbuf_free(response);
  pbuf_free(p);
}

static void setDNS(const char *name, struct ip_addr *ipaddr, void *arg) {
  struct Syncrophasor *pmuSync = (struct Syncrophasor *)arg;
  if (ipaddr != NULL) {
    if (ipaddr->addr != 0x00000000UL) {
      pmuSync->srv_IP.addr = ipaddr->addr;
      pmuSync->state = DNS_SOLVED;
#if 1
      uint32_t ip = pmuSync->srv_IP.addr;
      char tmp[100];
      sprintf(tmp, "SERVER IP: %lu.%lu.%lu.%lu\t0x%X\t%s\n",
              ip >> 0 & 0xFF,
              ip >> 8 & 0xFF,
              ip >> 16 & 0xFF,
              ip >> 24 & 0xFF,
              (unsigned int)ip, name);
      uart1Print(tmp);
#endif
      return; // OK
    }
  }
  pmuSync->state = IDLE;
  pmuSync->srv_IP.addr = 0x00000000UL;
  return; // ERROR
}
