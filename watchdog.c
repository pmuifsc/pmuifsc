#include "watchdog.h"
#include "stm32f4xx.h"
#include <stdio.h>
#include <stdarg.h>


void InitWatchdog(void)
{

RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG,ENABLE);

  WWDG_DeInit();
  WWDG_SetPrescaler(WWDG_Prescaler_8);
  WWDG_SetWindowValue(0x7F);
  WWDG_Enable(0x5F);

}

void FeedDog(void)
{
WWDG_SetCounter(0x7F);
}
