
#include "hw_uid.h"

void hw_uid_Init(void) {
  
}

uint32_t hw_uid0_31(void) {
  return *((uint32_t *)0x1FFF7A10);
}

uint32_t hw_uid32_63(void) {
  return *((uint32_t *)0x1FFF7A14);
}

uint32_t hw_uid64_95(void) {
  return *((uint32_t *)0x1FFF7A18);
}

uint64_t hw_uid0_63(void) {
  uint32_t id_0_31 = hw_uid0_31();
  uint32_t id_32_63 = hw_uid32_63();
  uint64_t id = ((uint64_t)id_32_63 << 32) | id_0_31;
  return id;
}
