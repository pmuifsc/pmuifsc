/*
* codigo_matlab.c
*
*  Created on: Jun 24, 2015
*      Author: Victor Santos
*/

#include "calculus.h"

#include <stdio.h>
#include "rtc.h"


static uint8_t iterations = 10;

//ok
double angle(Complex num) {
  // // fprintf(dfd,"\n**** FN: angle ****\n");
  double _angle = 0;
  _angle = atan2(num.imag, num.real);
  // // fprintf(dfd,"angle: %5.4f\n", _angle);
  // // fprintf(dfd,"--- saiu FN: angle ---\n");
  return _angle;
}

//ok
float rad2deg(double rad) {
  // // fprintf(dfd,"\n**** FN: rad2deg ****\n");
  double deg = 0;
  deg = rad / (M_PI / 180.0);
  // fprintf(dfd, "deg: %5.4f\n", deg);
  // fprintf(dfd,"--- saiu FN: rad2deg ---\n");
  return deg;
}

//ok
float corrigeQuadrante(Complex phasor, Complex phasor_n1) {
  // fprintf(dfd, "\n**** FN: corrigeQuadrante ****\n");
  float _dif = 0;

  _dif = angle(phasor) - angle(phasor_n1);
  if (_dif > M_PI) {
    _dif = _dif - 2*M_PI;
  } else if (_dif < -M_PI) {
    _dif = _dif + 2*M_PI;
  }
  // fprintf(dfd,"_dif: %5.4f\n", _dif);
  // fprintf(dfd,"--- saiu FN: corrigeQuadrante ---\n");
  return _dif;
}

Complex c_sum(Complex a, Complex b) {
  // fprintf(dfd,"\n**** FN: csum ****\n");
  Complex sum;
  sum.real = a.real + b.real;
  sum.imag = a.imag + b.imag;
  // fprintf(dfd, "%5.4f\n", sum.real);
  // fprintf(dfd, "%5.4f\n", sum.imag);
  // fprintf(dfd,"--- saiu FN: csum ---\n");
  return sum;
}

Complex c_mult(Complex a, Complex b) {
  // fprintf(dfd,"\n**** FN: cmult ****\n");
  Complex mult;
  mult.real = (a.real * b.real) - (a.imag * b.imag);
  mult.imag = (a.real * b.imag) + (a.imag * b.real);
  // fprintf(dfd, "%5.4f\n", mult.real);
  // fprintf(dfd, "%5.4f\n", mult.imag);
  // fprintf(dfd,"--- saiu FN: cmult ---\n");
  return mult;
}

Complex c_diff(Complex a, Complex b) {
  // fprintf(dfd,"\n**** FN: cdiff ****\n");
  Complex diff;
  diff.real = a.real - b.real;
  diff.imag = a.imag - b.imag;
  // fprintf(dfd, "%5.4f\n", diff.real);
  // fprintf(dfd, "%5.4f\n", diff.imag);
  // fprintf(dfd,"--- saiu FN: cdiff ---\n");
  return diff;
}

//ok
int dotDivisionScalar(float * result, float scalar_b, float * frequencia, int num_ciclos) {
  int i = 0;

  for(i=0;i< num_ciclos; i++) {
    *result = scalar_b / *frequencia;
  }
  return 0;
}

uint8_t phaseDetection(int16_t *samples) {
  uint16_t phaseCheck, n;
  float32_t sum;
  for (phaseCheck = 0; phaseCheck < 3; phaseCheck++) {
    sum = 0.0;
#if 0
    // resistor preset to 1.5 V
    for (n = phaseCheck; n < (3*NSAMPLES); n+=3) {
      if (samples[n] >= 2048)
        sum += samples[n] - 2048;
      else
        sum -= samples[n] - 2048;
    }
    // Verificar taxa
    if (p >= 200000) {
      break;
    }
#else
    // resistor preset to 0.0 V
    for (n = phaseCheck; n < (3*NSAMPLES); n+=3) {
        sum += samples[n];
    }
    // Verificar taxa
    if (sum >= 10000) {
      return phaseCheck;
    }
#endif
  }
  return -1;
}

static int k = 0;
static float32_t diffPhase = 0.0;

int powerLineEvaluate(int16_t *samples1, int16_t *samples2, PowerLineInfo *res) {
  static float freqAVG[3] = {F_FREQ,F_FREQ,F_FREQ};
  uint8_t phaseToCalc;

  diffPhase = 0;
  if (diffPhase <= (-M_PI)) {
    diffPhase += 2*M_PI;
  }
  else if (diffPhase > (M_PI)) {
    diffPhase -= 2*M_PI;
  }

  if (!k) {
    res->phasor[0].real = 0;
    res->phasor[0].imag = 0;
    res->phasor[1].real = 0;
    res->phasor[1].imag = 0;
    res->phasor[2].real = 0;
    res->phasor[2].imag = 0;
    res->freq = F_FREQ;
  }
  phaseToCalc = phaseDetection(samples2);
  if (phaseToCalc == -1)
    return -1;

  calcPhasorFreq(samples1, samples2, res, phaseToCalc);

  freqAVG[2] = freqAVG[1];
  freqAVG[1] = freqAVG[0];
  freqAVG[0] = res->freq;
  res->freq = freqAVG[0] * ( 7.0/15)
            + freqAVG[1] * ( 5.0/15)
            + freqAVG[2] * ( 3.0/15);

  return phaseToCalc;
}

//necessário trabalhar em diferentes valores de retorno para cada situação de erro
int calcPhasorFreq(int16_t *samples1, int16_t *samples2, PowerLineInfo *res, uint8_t phaseToCalc) {
  const float voltageFactor[] = {
  (TRANSFORM_RELATION_A * ADC_VOLTAGE_REF) / (ADC_VOLTAGE_RANGE * RMS_RELATION),
  (TRANSFORM_RELATION_B * ADC_VOLTAGE_REF) / (ADC_VOLTAGE_RANGE * RMS_RELATION),
  (TRANSFORM_RELATION_C * ADC_VOLTAGE_REF) / (ADC_VOLTAGE_RANGE * RMS_RELATION) };
  // variaveis alocadas para o calculo do fasor
  float32_t frequency = 60.0;
  float32_t N2;
  float32_t N1;
  Complex phasor;
  //variavel temporaria para guardar o resultado parcial da DFT
  Complex X;
  // variavel de contagem de amostras do ciclo (for N)
  int16_t n;
  // usadas no calculo
  float32_t dif;
  float32_t p;
  float32_t delt;
  float32_t temp3;
  float32_t n_comp;
  uint8_t q;
  float32_t tempAngle, factorAngle;

  X.real = 0.0;
  X.imag = 0.0;
  factorAngle = 2.0 * M_PI / NSAMPLES;
  tempAngle = 0.0;
  for (n = 0; n < NSAMPLES; n++) {
    X.real += samples2[(3 * n) + phaseToCalc] * arm_cos_f32(tempAngle + diffPhase);
    X.imag -= samples2[(3 * n) + phaseToCalc] * arm_sin_f32(tempAngle + diffPhase);
    tempAngle += factorAngle;
  }
  phasor.real = (double) (2.0/NSAMPLES) * X.real;
  phasor.imag = (double) (2.0/NSAMPLES) * X.imag;

  //calculo da frequencia
  if (k > 0) {
    dif = corrigeQuadrante(phasor, res->phasor[phaseToCalc]);
    frequency = F_FREQ + (dif / (2*M_PI*NSAMPLES*T_SAMPLE));

    N2 = ((float)NSAMPLES * F_FREQ) / frequency;
    N1 = NSAMPLES;
    q = 0; //contador, pra limitar as tentivas de recursividade
    delt = 0;
    p = 0;
    dif = 0;
    temp3 = 0;
    //condições de saída do algoritmo recursivo: erro menor que 1u e 10 iterações
    while (fabs(N2 - N1) > 0.000001 && q < iterations) {
      N1 = N2;
      if (N2 <= NSAMPLES) {  //se a frequencia for acima da nominal (janela menor)
        X.real = 0;
        X.imag = 0;
        factorAngle = 2.0 * M_PI / N2;
        tempAngle = 0.0;
        for (n = 0; n < floor(N2); n++) {
          X.real += samples2[(3 * n) + phaseToCalc] * arm_cos_f32(tempAngle + diffPhase);
          X.imag -= samples2[(3 * n) + phaseToCalc] * arm_sin_f32(tempAngle + diffPhase);
          tempAngle += factorAngle;
        }
        //n = n + 1; //tentativa de corrigir o erro de indice que leva a valores errados de p
        delt = (N2 - floor(N2));

        //testar o calculo do p, se dentro dos parenteses eh so k ou (k-1)
        p = samples2[(3 * (n - 1)) + phaseToCalc] + ((samples2[(3 * n) + phaseToCalc]) - samples2[(3 * (n - 1)) + phaseToCalc]) * delt ;

        X.real += p * delt * arm_cos_f32((float32_t) (2.0 * M_PI * (N2 - 1) / (N2)));
        X.imag -= p * delt * arm_sin_f32((float32_t) (2.0 * M_PI * (N2 - 1) / (N2)));

        phasor.real = (2.0 / N2) * X.real;
        phasor.imag = (2.0 / N2) * X.imag;
      } else {
        //a frequency for abaixo da nominal (janela maior)
        n_comp = N2 - NSAMPLES;  // numero de pontos a completar

        X.real = 0;
        X.imag = 0;
        delt = (N2 - floor(N2));

        //alterado do matlab, por causa do indice de k diferir do C
        //p = samples_vector[NSAMPLES * (k-1) - (int)floor(n_comp)] + (samples_vector[NSAMPLES * (k-1) - (int)floor(n_comp) + 1] - samples_vector[NSAMPLES * (k-1) - (int)floor(n_comp)]); //original do matlab
        {
          int16_t index = (NSAMPLES - (int)floor(n_comp)) * 3;
          int16_t prevPoint = samples1[index - 3 + phaseToCalc];
          int16_t nextPoint = samples1[index + phaseToCalc];
          if (index == NSAMPLES*3) {
            nextPoint = samples2[phaseToCalc];
          }
          p = prevPoint + ((nextPoint - prevPoint) * (1 - delt));
        }
        X.real += p * delt * arm_cos_f32((float32_t) (2.0 * M_PI * (0 - n_comp) / N2) + diffPhase);
        X.imag -= p * delt * arm_sin_f32((float32_t) (2.0 * M_PI * (0 - n_comp) / N2) + diffPhase);

        factorAngle = 2.0 * M_PI / N2;
        tempAngle = (0 - floor(n_comp)) * factorAngle;
        for (n = (0 - floor(n_comp)); n < 0; n++) {
          X.real += samples1[(3 * (n + NSAMPLES)) + phaseToCalc] * arm_cos_f32(tempAngle + diffPhase);
          X.imag -= samples1[(3 * (n + NSAMPLES)) + phaseToCalc] * arm_sin_f32(tempAngle + diffPhase);
          tempAngle += factorAngle;
        }
        tempAngle = 0.0;
        for (n = 0; n < NSAMPLES; n++) {
          X.real += samples2[(3 * n) + phaseToCalc] * arm_cos_f32(tempAngle + diffPhase);
          X.imag -= samples2[(3 * n) + phaseToCalc] * arm_sin_f32(tempAngle + diffPhase);
          tempAngle += factorAngle;
        }

        phasor.real = (2.0 / N2) * X.real;
        phasor.imag = (2.0 / N2) * X.imag;
      }
      //Calculo da nova frequencia
      dif = corrigeQuadrante(phasor, res->phasor[phaseToCalc]);
      frequency = F_FREQ + (dif / (2*M_PI*NSAMPLES*T_SAMPLE));
      N2 = NSAMPLES * F_FREQ / frequency;
      q++;
    }
  }
  k = 1;
  res->phasor[phaseToCalc].real = phasor.real * voltageFactor[phaseToCalc];
  res->phasor[phaseToCalc].imag = phasor.imag * voltageFactor[phaseToCalc];
  res->freq = frequency;

  Complex Y;
  float32_t pY;
  int phaseX;
  int phaseY;
  phaseX = (phaseToCalc + 1) % 3;
  X.real = 0;
  X.imag = 0;
  phaseY = (phaseToCalc + 2) % 3;
  Y.real = 0;
  Y.imag = 0;
  factorAngle = 2.0 * M_PI / N2;
  tempAngle = 0.0;

  for (n = 0; n < floor(N2); n++) {
    float32_t cos = arm_cos_f32(tempAngle);
    float32_t sin = arm_sin_f32(tempAngle);
    X.real += samples2[(3 * n) + phaseX] * cos;
    X.imag -= samples2[(3 * n) + phaseX] * sin;
    Y.real += samples2[(3 * n) + phaseY] * cos;
    Y.imag -= samples2[(3 * n) + phaseY] * sin;
    tempAngle += factorAngle;
  }

  delt = (N2 - floor(N2));

  //testar o calculo do p, se dentro dos parenteses eh so k ou (k-1)
  p  = samples2[(3 * (n - 1)) + phaseX] + ((samples2[(3 * n) + phaseX]) - samples2[(3 * (n - 1)) + phaseX]) * delt;
  pY = samples2[(3 * (n - 1)) + phaseY] + ((samples2[(3 * n) + phaseY]) - samples2[(3 * (n - 1)) + phaseY]) * delt;

  X.real += p * delt * arm_cos_f32((float32_t) (2.0 * M_PI * (N2 - 1) / (N2)));
  X.imag -= p * delt * arm_sin_f32((float32_t) (2.0 * M_PI * (N2 - 1) / (N2)));

  Y.real += pY * delt * arm_cos_f32((float32_t) (2.0 * M_PI * (N2 - 1) / (N2)));
  Y.imag -= pY * delt * arm_sin_f32((float32_t) (2.0 * M_PI * (N2 - 1) / (N2)));

  phasor.real = (2.0 / N2) * X.real;
  phasor.imag = (2.0 / N2) * X.imag;
  res->phasor[phaseX].real = phasor.real * voltageFactor[phaseToCalc];
  res->phasor[phaseX].imag = phasor.imag * voltageFactor[phaseToCalc];

  phasor.real = (2.0 / N2) * Y.real;
  phasor.imag = (2.0 / N2) * Y.imag;
  res->phasor[phaseY].real = phasor.real * voltageFactor[phaseToCalc];
  res->phasor[phaseY].imag = phasor.imag * voltageFactor[phaseToCalc];

  return phaseToCalc;
}

float32_t frequencyEstimation(int16_t *samples1, int16_t *samples2, char phase) {
  uint16_t avgFinal = 0, n;

  for (n = 252; n < 256; n++) {
    avgFinal += samples2[(3 * n) + phase];
  }

  return 0;
}

void lowPassFilter(uint16_t *sampi1, uint16_t *sampi2, uint16_t *sampo1, uint16_t *sampo2) {
#define SMP 1
  int16_t i, j, avg[3], tmp[3];
  avg[0] = 0;
  avg[1] = 0;
  avg[2] = 0;
  for (i = 127*3; i >= 0*3; i -= 1*3) {
    avg[0] += sampi2[i];
    avg[1] += sampi2[i+1];
    avg[2] += sampi2[i+2];
    if (i <= (127-SMP)*3) {
      tmp[0] = sampi2[i+SMP*3];
      tmp[1] = sampi2[i+SMP*3+1];
      tmp[2] = sampi2[i+SMP*3+2];
      sampo2[i+SMP*3] = avg[0]   / (SMP+1);
      sampo2[i+SMP*3+1] = avg[1] / (SMP+1);
      sampo2[i+SMP*3+2] = avg[2] / (SMP+1);
      avg[0] -= tmp[0];
      avg[1] -= tmp[1];
      avg[2] -= tmp[2];
    }
  }
  for (j = 127*3; j > (127-SMP)*3; j -= 1*3) {
    avg[0] += sampi1[j];
    avg[1] += sampi1[j+1];
    avg[2] += sampi1[j+2];
    tmp[0] = sampi2[i+SMP*3];
    tmp[1] = sampi2[i+SMP*3+1];
    tmp[2] = sampi2[i+SMP*3+2];
    sampo2[i+SMP*3] = avg[0]   / (SMP+1);
    sampo2[i+SMP*3+1] = avg[1] / (SMP+1);
    sampo2[i+SMP*3+2] = avg[2] / (SMP+1);
    avg[0] -= tmp[0];
    avg[1] -= tmp[1];
    avg[2] -= tmp[2];
    i -= 1*3;
  }
  for (i = (127-SMP)*3; i >= 0*3; i -= 1*3) {
    avg[0] += sampi1[i];
    avg[1] += sampi1[i+1];
    avg[2] += sampi1[i+2];
    if (i <= (127-SMP)*3) {
      tmp[0] = sampi1[i+SMP*3];
      tmp[1] = sampi1[i+SMP*3+1];
      tmp[2] = sampi1[i+SMP*3+2];
      sampo1[i+SMP*3] = avg[0]   / (SMP+1);
      sampo1[i+SMP*3+1] = avg[1] / (SMP+1);
      sampo1[i+SMP*3+2] = avg[2] / (SMP+1);
      avg[0] -= tmp[0];
      avg[1] -= tmp[1];
      avg[2] -= tmp[2];
    }
  }
  return;
}

void setIterations(uint8_t num) {
  iterations = num;
}

uint8_t getIterations(void) {
  return iterations;
}

void resetK() {
  k = 0;
}
