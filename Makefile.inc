#*****************************************************************************#
# Makefile.inc: Some variable definitions to build CMSIS Peripherals.         #
#-----------------------------------------------------------------------------#
# Jesús Alonso Fernández @doragasu, 2012                                      #
#*****************************************************************************#

# PART: The chip used for the build
# PART = STM32F407VG
PART = STM32F429ZI

####### Below Options should be YES or NO

# Link fatfs Drivers and Libraries
USE_FATFS=NO

# Test mode only run a user interface to access sd card through serial
FATFS_TESTMODE=NO


#########################################

# Prefix to the cross compiler
PREFIX = arm-none-eabi-
# Common stuff
CC = gcc
AR = ar
RM = rm
MKDIR = mkdir
LD = ld
OBJCOPY = objcopy
# Some directories
STINCDIR= $(ROOT)/Include
CMSISINCDIR= $(ROOT)/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include

# Flags for building stuff
COREFLAGS = -mthumb
COREFLAGS+= -march=armv7e-m
COREFLAGS+= -mcpu=cortex-m4
COREFLAGS+= -mfloat-abi=hard
COREFLAGS+= -mfpu=fpv4-sp-d16
COREFLAGS+= -mlittle-endian
COREFLAGS+= -mthumb-interwork

CFLAGS+= $(COREFLAGS)
CFLAGS+= -O3
CFLAGS+= -ffunction-sections
CFLAGS+= -fdata-sections
CFLAGS+= -std=c99
CFLAGS+= -Wall
CFLAGS+= -DPART_${PART}
CFLAGS+= -DARM_MATH_CM4
CFLAGS+= -D__FPU_PRESENT=1
CFLAGS+= -DUSE_STDPERIPH_DRIVER
CFLAGS+= -DSTM32F4XX
ifeq ($(PART),STM32F429ZI)
	CFLAGS+= -DSTM32F429_439xx
else ifeq ($(PART),STM32F407VG)
	CFLAGS+= -DSTM32F40_41xxx
endif
CFLAGS+= -DHSE_VALUE=8000000
CFLAGS+= -DPLL_M=8
CFLAGS+= -I$(CMSISINCDIR)
CFLAGS+= -I$(STINCDIR)
