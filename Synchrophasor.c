
#include "Synchrophasor.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "rtc.h"
#include "hw_timer2.h"
#include "hw_adc.h"
#include "calculus.h"

#define NUMBER_PHASORS                      3
#define NUMBER_ANALOGS                      0
#define NUMBER_DIGITAL                      0

#define NUMBER_COMBINED                     (NUMBER_PHASORS+NUMBER_ANALOGS+(16*NUMBER_DIGITAL))

// Common Frame type definitions
#define FRAME_TYPE_DATA                     0xAA01
#define FRAME_TYPE_HEADER                   0xAA11
#define FRAME_TYPE_CONFIG_1                 0xAA21
#define FRAME_TYPE_CONFIG_2                 0xAA31
#define FRAME_TYPE_CONFIG_3                 0xAA51
#define FRAME_TYPE_COMMAND                  0xAA62

// Common Frame Fraction of second 31-24 bits definitions
#define FRAME_FRACSEC_NORMAL                0x00000000
#define FRAME_FRACSEC_1NS                   0x01000000
#define FRAME_FRACSEC_10NS                  0x02000000
#define FRAME_FRACSEC_100NS                 0x03000000
#define FRAME_FRACSEC_1US                   0x04000000
#define FRAME_FRACSEC_10US                  0x05000000
#define FRAME_FRACSEC_100US                 0x06000000
#define FRAME_FRACSEC_1MS                   0x07000000
#define FRAME_FRACSEC_10MS                  0x08000000
#define FRAME_FRACSEC_100MS                 0x09000000
#define FRAME_FRACSEC_1S                    0x0A000000
#define FRAME_FRACSEC_10S                   0x0B000000
#define FRAME_FRACSEC_FAULT                 0x0F000000

#define FRAME_FRACSEC_LEAP_PENDING          0x10000000
#define FRAME_FRACSEC_LEAP_OCCURRED         0x20000000
#define FRAME_FRACSEC_LEAP_DIR_ADD          0x00000000
#define FRAME_FRACSEC_LEAP_DIR_DEL          0x40000000

// Config Frame Time Base definitions
#define FRAME_TIME_BASE_FLAGS()             (0x00 << 24)
#define FRAME_TIME_BASE(base  )             (base & 0x00FFFFFF)

// Config Frame Format definitions
#define FRAME_FORMAT_PHASORS_RECTANGULAR    0x0000
#define FRAME_FORMAT_PHASORS_POLAR          0x0001
#define FRAME_FORMAT_PHASORS_INTEGER        0x0000
#define FRAME_FORMAT_PHASORS_FLOAT          0x0002
#define FRAME_FORMAT_ANALOGS_INTEGER        0x0000
#define FRAME_FORMAT_ANALOGS_FLOAT          0x0004
#define FRAME_FORMAT_FREQ_INTEGER           0x0000
#define FRAME_FORMAT_FREQ_FLOAT             0x0008

// Config Frame Phasor conversion factor definitions
#define FRAME_PHUNIT_VOLTAGE                0x00000000
#define FRAME_PHUNIT_CURRENT                0x01000000
#define FRAME_PHUNIT_SCALE(max)             (((uint32_t)((float)max*(100000.0/32768.0))) & 0x00FFFFFF)

// Config Frame Analog conversion factor definitions
#define FRAME_ANUNIT_SINGLE_POINT           0x00000000
#define FRAME_ANUNIT_RMS                    0x01000000
#define FRAME_ANUNIT_PEAK                   0x02000000
#define FRAME_ANUNIT_SCALE(max)             (((uint32_t)((float)max*(100000.0/32768.0))) & 0x00FFFFFF)

// Config Frame Digital mask
#define FRAME_DGUNIT_NORMAL_OP(cond)        (cond<<16)

// Config Frame Nominal line frequency definitions
#define FRAME_FNOM_60HZ                     0x0000
#define FRAME_FNOM_50HZ                     0x0001

// Config Frame Data rate definitions
#define FRAME_DATA_RATE_PER_SECOND(rate)    (rate)
#define FRAME_SECOND_PER_DATA(rate)         (-rate)

// Data Frame Stat definitions
#define FRAME_STAT_TRIGGER_MANUAL           0x0000
#define FRAME_STAT_TRIGGER_MAG_LOW          0x0001
#define FRAME_STAT_TRIGGER_MAG_HIGH         0x0002
#define FRAME_STAT_TRIGGER_ANGLE_DIFF       0x0003
#define FRAME_STAT_TRIGGER_FREQ_HL          0x0004
#define FRAME_STAT_TRIGGER_DF_DT_HIGH       0x0005
#define FRAME_STAT_TRIGGER_DIGITAL          0x0007
#define FRAME_STAT_UNLOCKED_TIME_10S        0x0000
#define FRAME_STAT_UNLOCKED_TIME_100S       0x0010
#define FRAME_STAT_UNLOCKED_TIME_1000S      0x0020
#define FRAME_STAT_UNLOCKED_TIME_INF        0x0030
#define FRAME_STAT_TIME_QUALITY_NO_USE      0x0000
#define FRAME_STAT_TIME_QUALITY_100NS       0x0040
#define FRAME_STAT_TIME_QUALITY_1US         0x0080
#define FRAME_STAT_TIME_QUALITY_10US        0x00C0
#define FRAME_STAT_TIME_QUALITY_100US       0x0100
#define FRAME_STAT_TIME_QUALITY_1MS         0x0140
#define FRAME_STAT_TIME_QUALITY_10MS        0x0180
#define FRAME_STAT_TIME_QUALITY_UNKNOW      0x01C0
#define FRAME_STAT_DATA_NOT_MODIFIED        0x0000
#define FRAME_STAT_DATA_MODIFIED            0x0200
#define FRAME_STAT_CONFIG_CHANGE_OK         0x0000
#define FRAME_STAT_CONFIG_CHANGE_INCOMMING  0x0400
#define FRAME_STAT_NO_TRIGGER               0x0000
#define FRAME_STAT_TRIGGED                  0x0800
#define FRAME_STAT_SORTING_STAMP            0x0000
#define FRAME_STAT_SORTING_ARRIVAL          0x1000
#define FRAME_STAT_SYNCHRONIZED             0x0000
#define FRAME_STAT_SYNC_ERROR               0x2000
#define FRAME_STAT_DATA_ERROR_NO_ERROR      0x0000
#define FRAME_STAT_DATA_ERROR_NO_DATA       0x4000
#define FRAME_STAT_DATA_ERROR_TEST          0x8000
#define FRAME_STAT_DATA_ERROR_ERROR         0xC000

// Temp buffer
uint8_t config_frame[2048];

// setup functions
void setIDCODE(uint32_t id);
void setStationName(const char *name);
static uint32_t __inline__ getSOC(void);
static uint32_t __inline__ getFracSec(void);
static uint32_t __inline__ getQoTmessage(void);
uint8_t *getConfigurationFrame(uint16_t *length);
uint8_t *getDataFrame(uint16_t *length, PowerLineInfo *lineData);
uint16_t computeCRC(const uint8_t *message, uint16_t len);

// common copy functions
static void __inline__ encode2B(uint8_t *buffer, uint16_t *length, uint16_t data);
static void __inline__ encode4B(uint8_t *buffer, uint16_t *length, uint32_t data);
// configuration functions
static void __inline__ encodeConfig_FrameSize(uint8_t *buffer, uint16_t *length);
static void __inline__ encodeConfig_STN(uint8_t *buffer, uint16_t *length);
static void encodeConfig_Chnam(uint8_t *buffer, uint16_t *length);
static void encodeConfig_PhUnit(uint8_t *buffer, uint16_t *length);
static void encodeConfig_AnUnit(uint8_t *buffer, uint16_t *length);
static void encodeConfig_DigUnit(uint8_t *buffer, uint16_t *length);
// data functions
static uint32_t __inline__ getSampleSOC(PowerLineInfo *res);
static uint32_t __inline__ getSampleFracSec(PowerLineInfo *res);
static void encodeData_Phasors(uint8_t *buffer, uint16_t *length, PowerLineInfo *lineData);
static void __inline__ encodeData_Freq(uint8_t *buffer, uint16_t *length, PowerLineInfo *lineData);
static void __inline__ encodeData_DFreq(uint8_t *buffer, uint16_t *length, PowerLineInfo *lineData);
static uint16_t __inline__ getStatUnlockedTime(void);
static void encodeData_Analog(uint8_t *buffer, uint16_t *length);
static void encodeData_Digital(uint8_t *buffer, uint16_t *length);

// Configuration variables
// -- commom frame
// PMU ID
uint16_t idcode = 1;
// Auxiliary variable to checksum
uint16_t chk;

// -- configuration frame
// timer2 period
uint32_t time_base = FRAME_TIME_BASE_FLAGS() | FRAME_TIME_BASE(TIMER2_BASE);
const uint16_t num_pmu = 1;   // Number of pmu send in data frame

// Name of station
char stn[16] = {'P','M','U',' ','I','F','S','C',' ','V','1','.','0',' ',' ',' '};
#define idcode_data idcode
const uint16_t format = FRAME_FORMAT_PHASORS_RECTANGULAR | FRAME_FORMAT_PHASORS_FLOAT | FRAME_FORMAT_ANALOGS_FLOAT | FRAME_FORMAT_FREQ_FLOAT;

// Amount of phasor, analog and digital channels
const uint16_t phnmr = NUMBER_PHASORS;
const uint16_t annmr = NUMBER_ANALOGS;
const uint16_t dgnmr = NUMBER_DIGITAL;

// Channel names, fixed yet
const char chnam[NUMBER_COMBINED][16] = {
  {'V','A',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
  {'V','B',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
  {'V','C',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '}
};

// Factor scale used in phasors
const uint32_t phunit[NUMBER_PHASORS] = {
  0x00000000,
  0x00000000,
  0x00000000
};

// Factor scale used in analog channels
const uint32_t anunit[NUMBER_ANALOGS] = {
};

// Mask and normal value used in digital channels
const uint32_t digunit[NUMBER_DIGITAL] = {
};

// Nominal line frequency
const uint32_t fnom = FRAME_FNOM_60HZ;

// Config frame changes couter
uint16_t cfgcnt = 0;
// Changed flag
char configChanged = 0;

// Rate of phasor data transmission
const int16_t data_rate = FRAME_DATA_RATE_PER_SECOND(60);


// -- data frame
// Bit mapped status flags
uint16_t stat = FRAME_STAT_DATA_ERROR_NO_ERROR |
                FRAME_STAT_SYNCHRONIZED |
                FRAME_STAT_SORTING_STAMP |
                FRAME_STAT_NO_TRIGGER |
                FRAME_STAT_CONFIG_CHANGE_OK |
                FRAME_STAT_DATA_NOT_MODIFIED |
                FRAME_STAT_TIME_QUALITY_10US /*|
                FRAME_STAT_UNLOCKED_TIME_10S */; // Set in Data frame

//



// Set PMU values
void setIDCODE(uint32_t id) {
  if (id == 0 || id == 65535) {
    return;
  }
  idcode = id;
  configChanged = 1;
}

void setStationName(const char *name) {
  char i;
  for (i = 0; i < 16; i++) {
    if (*name == '\0') {
      stn[i] = ' ';
    } else {
      stn[i] = *(name++);
    }
  }
  configChanged = 1;
}

static uint32_t __inline__ getSOC() {
  return rtc_getSOC();
}

static uint32_t __inline__ getFracSec() {

  uint32_t time;
  time = hw_timer2_read();
  time = time & 0x00FFFFFF;
  time |= (uint32_t)getQoTmessage();
  return time;
}

static uint32_t __inline__ getSampleSOC(PowerLineInfo *res) {
  uint32_t SOC = res->timestamp;

  return SOC;
}

static uint32_t __inline__ getSampleFracSec(PowerLineInfo *res) {
  uint32_t frac = res->fracsec;
  frac = frac & 0x00FFFFFF;
    frac |= (uint32_t)getQoTmessage();
  return frac;
}

static uint16_t __inline__ getStatUnlockedTime(void) {
  uint16_t lockedTime = rtc_getLastSync();
  if (lockedTime < 10) {
    return FRAME_STAT_UNLOCKED_TIME_10S;
  }
  else if (lockedTime < 100) {
    return FRAME_STAT_UNLOCKED_TIME_100S;
  }
  else if (lockedTime < 1000) {
    return FRAME_STAT_UNLOCKED_TIME_1000S;
  }
  else {
    return FRAME_STAT_UNLOCKED_TIME_INF;
  }
}

static uint32_t __inline__ getQoTmessage(void) {
  uint32_t QoTbyte = 0;
  // Must treat leap second
  if (!rtc_lockedUTC()) {
    if (rtc_getSOCRef() < 1472688000 || hw_timer2_opStatus()) { // 01/09/2016 00:00:00
      QoTbyte |= FRAME_FRACSEC_FAULT; // Not initialized
    } else {
      QoTbyte |= FRAME_FRACSEC_10US;
    }
  }
  /* else {
    QoTbyte |= FRAME_FRACSEC_NORMAL; // FRAME_FRACSEC_NORMAL is a alias for 0
  } // else not necessary due to QoTbyte isn't changed */
  return QoTbyte;
}

uint8_t *getConfigurationFrame(uint16_t *length) {
  if (configChanged) {
    configChanged = 0;
    cfgcnt++;
  }
  *length = 0;
  uint8_t pmu_count;
  encode2B(config_frame, length, FRAME_TYPE_CONFIG_2);  // Set SYNC field
  // Increment length but real length is unknow, must be called after Sync
  encodeConfig_FrameSize(config_frame, length);
  encode2B(config_frame, length, idcode);               // Set IDCODE field
  uint32_t SOCVal, fracVal;
  SOCVal = getSOC();
  fracVal = getFracSec();
  // Check if second was changed
  if (SOCVal != getSOC()) {
    if (fracVal < 500000) {
      SOCVal = getSOC();
    }
  }
  encode4B(config_frame, length, SOCVal);               // Set SOC field
  encode4B(config_frame, length, fracVal);              // Set FRACSEC field
  encode4B(config_frame, length, time_base);            // Set TIME_BASE field
  encode2B(config_frame, length, num_pmu);              // Set NUM_PMU field
  encodeConfig_STN(config_frame, length);
  encode2B(config_frame, length, idcode_data);          // Set IDCODE field
  encode2B(config_frame, length, format);               // Set FORMAT field
  encode2B(config_frame, length, phnmr);                // Set PHNMR field
  encode2B(config_frame, length, annmr);                // Set ANNMR field
  encode2B(config_frame, length, dgnmr);                // Set DGNMR field
  encodeConfig_Chnam(config_frame, length);
  encodeConfig_PhUnit(config_frame, length);
  encodeConfig_AnUnit(config_frame, length);
  encodeConfig_DigUnit(config_frame, length);
  encode2B(config_frame, length, fnom);                 // Set FNOM field
  encode2B(config_frame, length, cfgcnt);               // Set CFGCNT field
  for (pmu_count = 1; pmu_count < num_pmu; pmu_count++) {
    // For others data stream, not supported yet
  }
  encode2B(config_frame, length, data_rate);            // Set DATA_RATE field
  // Set real length
  encodeConfig_FrameSize(config_frame, length);

  chk = computeCRC(config_frame, *length);
  encode2B(config_frame, length, chk);                  // Set CHK field

#if 0
  {
    char tmp[20];
    int i;
    sprintf(tmp, "%d\n", length);
    uart1Print(tmp);
    for (i = 0; i < length; i++) {
      sprintf(tmp, "%02X, ", config_frame[i]);
      uart1Print(tmp);
      if ((i % 16) == 15) {
        int k;
        for (k = 0; k < 65533; k++) {
          hw_uart1_read(config_frame);
        }
        hw_uart1_send('\n');
      }
    }
    while (1);
  }
#endif

  return config_frame;
}

uint8_t *getDataFrame(uint16_t *length, PowerLineInfo *lineData) {
  *length = 0;
  uint8_t pmu_count;
  encode2B(config_frame, length, FRAME_TYPE_DATA);      // Set SYNC field
  // Increment length but real length is unknow, must be called after Sync
  encodeConfig_FrameSize(config_frame, length);
  encode2B(config_frame, length, idcode);               // Set IDCODE field
  encode4B(config_frame, length, getSampleSOC(lineData));       // Set SOC field
  encode4B(config_frame, length, getSampleFracSec(lineData));   // Set FRACSEC field
  // Must define stat changes, like GPS sync signal error
  uint16_t tmpStat = stat | getStatUnlockedTime();
  encode2B(config_frame, length, tmpStat);              // Set STAT field
  encodeData_Phasors(config_frame, length, lineData);
  encodeData_Freq(config_frame, length, lineData);
  encodeData_DFreq(config_frame, length, lineData);
  encodeData_Analog(config_frame, length);
  encodeData_Digital(config_frame, length);
  for (pmu_count = 1; pmu_count < num_pmu; pmu_count++) {
    // For others data stream, not supported yet
  }
  // Set real length
  encodeConfig_FrameSize(config_frame, length);

  chk = computeCRC(config_frame, *length);
  encode2B(config_frame, length, chk);                  // Set CHK field
  return config_frame;
}

// Commom Encode functions
static void __inline__ encode2B(uint8_t *buffer, uint16_t *length, uint16_t data) {
  uint8_t *tmpAddr = &buffer[(*length)+1];
  uint8_t i;
  for (i = 0; i < 2; i++) {
    *(tmpAddr--) = data & 0xFF;
    data = data >> 8;
  }
  *length = *(length) + 2;
  return;
}

static void __inline__ encode4B(uint8_t *buffer, uint16_t *length, uint32_t data) {
  uint8_t *tmpAddr = &buffer[(*length)+3];
  uint8_t i;
  for (i = 0; i < 4; i++) {
    *(tmpAddr--) = data & 0xFF;
    data = data >> 8;
  }
  *length = *(length) + 4;
  return;
}

// Encode config functions
static void __inline__ encodeConfig_FrameSize(uint8_t *buffer, uint16_t *length) {
  uint16_t tmpLen = *length;
  if (tmpLen == 2) {
    buffer[tmpLen++] = 0;
    buffer[tmpLen++] = 0;
    *length = 4;
  } else {
    tmpLen += 2; // checksum lenght
    buffer[2] = (tmpLen >> 8) & 0xFF;
    buffer[3] = (tmpLen >> 0) & 0xFF;
  }
  return;
}

static void __inline__ encodeConfig_STN(uint8_t *buffer, uint16_t *length) {
  uint16_t tmpLen = *length;
  uint16_t i;
  for (i = 0; i < 16; i++) {
    buffer[tmpLen+i] = stn[i];
  }
  *length = tmpLen + 16;
  return;
}

static void encodeConfig_Chnam(uint8_t *buffer, uint16_t *length) {
  uint16_t tmpLen = *length;
  uint16_t i, j, total;
  total = phnmr + annmr + (dgnmr * 16);
  for (j = 0; j < total; j++) {
    for (i = 0; i < 16; i++) {
      buffer[tmpLen++] = chnam[j][i];
    }
  }
  *length = tmpLen;
  return;
}

static void encodeConfig_PhUnit(uint8_t *buffer, uint16_t *length) {
  uint16_t tmpLen = *length;
  uint16_t j;
  for (j = 0; j < phnmr; j++) {
    buffer[tmpLen++] = (phunit[j] >> 24) & 0xFF;
    buffer[tmpLen++] = (phunit[j] >> 16) & 0xFF;
    buffer[tmpLen++] = (phunit[j] >>  8) & 0xFF;
    buffer[tmpLen++] = (phunit[j] >>  0) & 0xFF;
  }
  *length = tmpLen;
  return;
}

static void encodeConfig_AnUnit(uint8_t *buffer, uint16_t *length) {
  uint16_t tmpLen = *length;
  uint16_t j;
  for (j = 0; j < annmr; j++) {
    buffer[tmpLen++] = (anunit[j] >> 24) & 0xFF;
    buffer[tmpLen++] = (anunit[j] >> 16) & 0xFF;
    buffer[tmpLen++] = (anunit[j] >>  8) & 0xFF;
    buffer[tmpLen++] = (anunit[j] >>  0) & 0xFF;
  }
  *length = tmpLen;
  return;
}

static void encodeConfig_DigUnit(uint8_t *buffer, uint16_t *length) {
  uint16_t tmpLen = *length;
  uint16_t j;
  for (j = 0; j < dgnmr; j++) {
    buffer[tmpLen++] = (digunit[j] >> 24) & 0xFF;
    buffer[tmpLen++] = (digunit[j] >> 16) & 0xFF;
    buffer[tmpLen++] = (digunit[j] >>  8) & 0xFF;
    buffer[tmpLen++] = (digunit[j] >>  0) & 0xFF;
  }
  *length = tmpLen;
  return;
}

// Encode data functions

uint32_t swap(uint32_t num) {
  return ((num>>24)&0xff) | // move byte 3 to byte 0
         ((num<<8)&0xff0000) | // move byte 1 to byte 2
         ((num>>8)&0xff00) | // move byte 2 to byte 1
         ((num<<24)&0xff000000); // byte 0 to byte 3
}

static void encodeData_Phasors(uint8_t *buffer, uint16_t *length, PowerLineInfo *lineData) {
  float floatPart1, floatPart2;
  uint8_t phasorCount;
  for (phasorCount = 0; phasorCount < phnmr; phasorCount++) {
    if (format & 0x0001) {
      // polar form
      // need convert phasor data to polar form and save in floatPart1, floatPart2
      // Convert magnitude
      // Convert angle, in radians, and set between -pi < x <= pi
    } else {
      // rectangular form
      floatPart1 = lineData->phasor[phasorCount].real;
      floatPart2 = lineData->phasor[phasorCount].imag;
    }
    if (format & 0x0002) {
      // Floating point form
      encode4B(buffer, length, *((uint32_t *)&floatPart1));
      encode4B(buffer, length, *((uint32_t *)&floatPart2));
    } else {
      // 16-bit integer form
//       uint16_t intPart1, intPart2;
      // Must convert data to integer using phunit conversion factor
    }
  }
}

static void __inline__ encodeData_Freq(uint8_t *buffer, uint16_t *length, PowerLineInfo *lineData) {
  if (format & 0x0008) {
#if 0
    // Floating point form
    uint8_t nominalfreq = (fnom == FRAME_FNOM_60HZ)? 60 : 50;
    float freqDev = lineData->freq - nominalfreq;
#else
    float freqDev = lineData->freq;
#endif
    encode4B(buffer, length, *((uint32_t *)&freqDev));
  } else {
    // 16-bit integer form
    // Must implement
  }
}

static void __inline__ encodeData_DFreq(uint8_t *buffer, uint16_t *length, PowerLineInfo *lineData) {
  static float prevFreq = 0.0;
  if (prevFreq == 0.0) {
    prevFreq = lineData->freq;
  }
  if (format & 0x0008) {
    // Floating point form
    float nominalfreq = (fnom == FRAME_FNOM_60HZ)? 60.0 : 50.0;
    float dfreq = (lineData->freq - prevFreq) * nominalfreq;
    encode4B(buffer, length, *((uint32_t *)&dfreq));
  } else {
    // 16-bit integer form
    // Must implement
  }
  prevFreq = lineData->freq;
}

static void encodeData_Analog(uint8_t *buffer, uint16_t *length) {
  uint8_t analogCount;
  for (analogCount = 0; analogCount < annmr; analogCount++) {
    if (format & 0x0004) {
      // Floating point form
      // Must implement
    } else {
      // 16-bit integer form
      // Must implement
    }
  }
}

static void encodeData_Digital(uint8_t *buffer, uint16_t *length) {
  uint8_t digitalCount;
  for (digitalCount = 0; digitalCount < dgnmr; digitalCount++) {
    // Must implement
  }
}

// CRC-CCITT checksum function
uint16_t computeCRC(const uint8_t *message, uint16_t len) {
  uint16_t crc = 0xFFFF;
  uint16_t temp;
  uint16_t quick;
  uint32_t i;
  for (i = 0; i < len; i++) {
    temp = (crc >> 8) ^ message[i];
    crc <<= 8;
    quick = temp ^ (temp >> 4);
    crc ^= quick;
    quick <<= 5;
    crc ^= quick;
    quick <<= 7;
    crc ^= quick;
  }
  return crc;
}
