
#include "hw_gpio.h"

void hw_gpio_Init(GPIO_TypeDef* GPIOx, uint32_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode) {
  // TODO: Implement Interrputs and/or DMA
  // TODO: Implement hw_SPI1 to handle peripheral
  // Initialize peripheral clock
  
  // Initialize GPIO_Interface
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_StructInit(&GPIO_InitStruct);
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin;
  GPIO_Init(GPIOx, &GPIO_InitStruct);
  
  return;
}

void hw_gpio_set(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
  GPIO_SetBits(GPIOx, GPIO_Pin);
}

void hw_gpio_reset(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
  GPIO_ResetBits(GPIOx, GPIO_Pin);
}
